// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// C++ TLBWRTR : $Revision:   1.151.1.0.1.27  $
// File generated on 13.12.2014 16:53:21 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Documents\Visual Studio 2010\Projects\Integrator\IntServ\IntegratorServer.tlb (1)
// LIBID: {80613DD3-903C-4C4C-807D-A6C2B9B5A20D}
// LCID: 0
// Helpfile: 
// HelpString: Project1 Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#include "IntegratorServer_TLB.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Integratorserver_tlb
{


// *********************************************************************//
// GUIDS declared in the TypeLibrary                                      
// *********************************************************************//
//*********************************************************************//
// NOTE: This file is no longer used. GUIDS previously defined in this 
// file are now defined in the header itself, using __declspec(selectany).
//********************************************************************//

};     // namespace Integratorserver_tlb

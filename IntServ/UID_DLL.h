
#ifndef UID_DLLH
#define UID_DLLH

#if defined(BUILD_DLL)
#  define DLL_EXP __declspec(dllexport)
#else
#  define DLL_EXP __declspec(dllimport)
#endif

enum Ein   {I,U};//I-���, U-����������
enum Esinc {NOSIN, SIN};//������� �������������
enum Ediv  {D1_1, D1_8};//���������� �������� ������� (1:1, 1:8)
enum Egain {X1,X2,X4,X5,X8,X10,X16,X32};//�������� PGA
enum Econd {C2,C220};//�����������: C2 - 2��, C220 - 220��
enum Epol  {P,N};//����������
enum Etest {NOTEST,TEST};//����.��������
enum Emode {MES_A,MES_D};//����� ��������� (MES_A - ���������, MES_D - ����)
enum Esec  {US,MS};//US-�����, MS-����

//! ���������������� ���������
struct Config 
{
Ein       in;///I-���, U-����������
Esinc     sinc;///������� �������������
Ediv      div;//���������� �������� ������� (1:1, 1:8)
Egain     gain;///�������� PGA
Econd     cond;///�����������: C2 - 2��, C220 - 220��
Epol      pol;///����������
Etest     test;///����.��������
Emode     mode;///����� ��������� (MES_A - ���������, MES_D - ����)
Esec      umsec;///US-�����, MS-����
unsigned short time;///����� �������������� (����� ��� ����)
unsigned short dose;///��������� ���� (� ������ ���������� �����������)
double    sense;///������. ������� (A/(�*��2) ��� ����,A/(�*��2) ��� ����������)
double    charge;///���������� �����
};

//! ���������� ���������
struct Result 
{
unsigned adc_data;///��� ���
double  u;///���������� (�)
double  i;///��� (�)
unsigned d;///���� � ������ ����������
double  skv;///����������
double  fdoza;///���� (1/��2)
double  j_mid;///������� �����.���� (A/��2)
double  j_imp;///�����. ���� � �������� (A/��2)
double  q;///����� (�)
};
//---------------------------------------------------------------------------
extern "C"
{
//! �������� ������������ COM-�����
	/*!
		\return ���������� ������������� ������������ COM-�����, ����� ������� ���� ����� �����������. 
		���� ������� ������������ COM-����� �� ����������, ��� ���������� �� ����������, ���������� ����
		*/
HANDLE DLL_EXP __cdecl OpenCom();

//! ��������� ���������
	/*!
		��������� ������� ��������������� ���������
		\param [in] conf - ��������� �� ��������� ���������������� ����������
		\param [out] res - ��������� �� ��������� ���������� �������
		\return ���������� 0 � ������ ��������� ���������, 1 ��� ������������ ������� ���� 
		Config.umsec, 2 ��� ���������� ���������������, -1 ���� COM-���� �� ������
		*/
int DLL_EXP __cdecl Measure(Config *conf, Result *res);

//! ������� ����
	/*!
		��������� � ������������ ������� ����, ������������� � �������� ����������� � ���������� �������� ����, 
		����������� � ������� �������� ����� �������. ��� ����������� ������ � ������� ���������� stop 
		��������� ����������� � �������� ���������� ����. ��� ������� � ��������� ���������� stop �������� 
		�������������� ���������� �������� ����������� (����������� �������, �������������� ������ ���������� ���������� ������� ����). 
		\param [in] conf - ��������� �� ��������� ���������������� ����������
		\param [in] stop - �������� ���������� ���� (������� ��������), ���������� �������� ����������� (��������� ��������)
		\return ���������� ���� ����������� (� ������ ����������� ������������). ����� ���������� -2 ��� ���������� 
		�������� ����, -3 ��� �������������� ����������, -1 ���� COM-���� �� ������. 
		*/
int DLL_EXP __cdecl Dose(Config *conf, int stop= 0);

//! ������������� ��������� / �������� ����������
	/*!
		\param [in] conf - ��������� �� ��������� ���������������� ����������
		\return ���������� 0 � ������ ������ � 2 ��� ���������� ���������������, -1 ���� COM-���� �� ������.
		*/
int DLL_EXP __cdecl Auto(Config *conf);
}
//---------------------------------------------------------------------------
#endif

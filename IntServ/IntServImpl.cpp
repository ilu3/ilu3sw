// INTSERVIMPL : Implementation of TIntServImpl (CoClass: IntServ, Interface: IIntServ)

#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"

#include "INTSERVIMPL.H"

/////////////////////////////////////////////////////////////////////////////
// TIntServImpl

STDMETHODIMP TIntServImpl::OpenComServer(int* result)
{
        HANDLE h=OpenCom();
        if(h==0)
        {
                try
                {
                        *result=0;
                }
                catch(Exception &e)
                {
                        return Error(e.Message.c_str(), IID_IIntServ);
                }
        }
        else
        {
                try
                {
                        *result=(int)h;
                }
                catch(Exception &e)
                {
                        return Error(e.Message.c_str(), IID_IIntServ);
                }
        }
        return S_OK;
}

STDMETHODIMP TIntServImpl::MeasureServer(unsigned in, unsigned sinc,
  unsigned div, unsigned gain, unsigned cond, unsigned pol, unsigned test,
  unsigned mode, unsigned umsec, unsigned_short time, unsigned_short dose,
  double sense, double charge, unsigned* adc_data, double* u, double* i,
  unsigned* d, double* skv, double* fdoza, double* j_mid, double* j_imp,
  double* q)
{
        Config conf;
        Result res;
        try
        {
                conf.in=in;
                conf.sinc=sinc;
                conf.div=div;
                conf.gain=gain;
                conf.cond=cond;
                conf.pol=pol;
                conf.test=test;
                conf.mode=mode;
                conf.umsec=umsec;
                conf.time=time;
                conf.dose=dose;
                conf.sense=sense;
                conf.charge=charge;
        }
        catch(Exception &e)
        {
                return Error(e.Message.c_str(), IID_IIntServ);
        }
        Measure(&conf,&res);
        try
                {
                      *adc_data=res.adc_data;
                      *u=res.u;
                      *i=res.i;
                      *d=res.d;
                      *skv=res.skv;
                      *fdoza=res.fdoza;
                      *j_mid=res.j_mid;
                      *j_imp=res.j_imp;
                      *q=res.q;
                }
                catch(Exception &e)
                {
                        return Error(e.Message.c_str(), IID_IIntServ);
                }
        return S_OK;
}

STDMETHODIMP TIntServImpl::AutoServer(unsigned in, unsigned sinc,
  unsigned div, unsigned gain, unsigned cond, unsigned pol, unsigned test,
  unsigned mode, unsigned umsec, unsigned_short time, unsigned_short dose,
  double sense, double charge, int* result)
{
        Config conf;
        try
        {
                conf.in=in;
                conf.sinc=sinc;
                conf.div=div;
                conf.gain=gain;
                conf.cond=cond;
                conf.pol=pol;
                conf.test=test;
                conf.mode=mode;
                conf.umsec=umsec;
                conf.time=time;
                conf.dose=dose;
                conf.sense=sense;
                conf.charge=charge;
                int r=Auto(&conf);
                *result=r;
        }
        catch(Exception &e)
        {
                return Error(e.Message.c_str(), IID_IIntServ);
        }
        return S_OK;
}

STDMETHODIMP TIntServImpl::DoseServer(unsigned in, unsigned sinc,
  unsigned div, unsigned gain, unsigned cond, unsigned pol, unsigned test,
  unsigned mode, unsigned umsec, unsigned_short time, unsigned_short dose,
  double sense, double charge, int stop, int* result)
{
        Config conf;
        try
        {
                conf.in=in;
                conf.sinc=sinc;
                conf.div=div;
                conf.gain=gain;
                conf.cond=cond;
                conf.pol=pol;
                conf.test=test;
                conf.mode=mode;
                conf.umsec=umsec;
                conf.time=time;
                conf.dose=dose;
                conf.sense=sense;
                conf.charge=charge;
                int r=Dose(&conf,stop);
                *result=r;
        }
        catch(Exception &e)
        {
                return Error(e.Message.c_str(), IID_IIntServ);
        }
        return S_OK;
}



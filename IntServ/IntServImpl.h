// INTSERVIMPL.H : Declaration of the TIntServImpl

#ifndef IntServImplH
#define IntServImplH

#define ATL_APARTMENT_THREADED

#include "IntegratorServer_TLB.h"

#include "UID_DLL.h"
#include "Unit1.h"
/////////////////////////////////////////////////////////////////////////////
// TIntServImpl     Implements IIntServ, default interface of IntServ
// ThreadingModel : Apartment
// Dual Interface : TRUE
// Event Support  : FALSE
// Default ProgID : Project1.IntServ
// Description    : 
/////////////////////////////////////////////////////////////////////////////
class ATL_NO_VTABLE TIntServImpl : 
  public CComObjectRootEx<CComSingleThreadModel>,
  public CComCoClass<TIntServImpl, &CLSID_IntServ>,
  public IDispatchImpl<IIntServ, &IID_IIntServ, &LIBID_IntegratorServer>
{
public:
  TIntServImpl()
  {
        
  }

  // Data used when registering Object 
  //
  DECLARE_THREADING_MODEL(otApartment);
  DECLARE_PROGID("IntegratorServer.IntServ");
  DECLARE_DESCRIPTION("");

  // Function invoked to (un)register object
  //
  static HRESULT WINAPI UpdateRegistry(BOOL bRegister)
  {
    TTypedComServerRegistrarT<TIntServImpl> 
    regObj(GetObjectCLSID(), GetProgID(), GetDescription());
    return regObj.UpdateRegistry(bRegister);
  }


BEGIN_COM_MAP(TIntServImpl)
  COM_INTERFACE_ENTRY(IIntServ)
  COM_INTERFACE_ENTRY2(IDispatch, IIntServ)
END_COM_MAP()

// IIntServ
public:
  STDMETHOD(OpenComServer(int* result));
  STDMETHOD(MeasureServer(unsigned in, unsigned sinc, unsigned div,
      unsigned gain, unsigned cond, unsigned pol, unsigned test,
      unsigned mode, unsigned umsec, unsigned_short time,
      unsigned_short dose, double sense, double charge, unsigned* adc_data,
      double* u, double* i, unsigned* d, double* skv, double* fdoza,
      double* j_mid, double* j_imp, double* q));
  STDMETHOD(AutoServer(unsigned in, unsigned sinc, unsigned div,
      unsigned gain, unsigned cond, unsigned pol, unsigned test,
      unsigned mode, unsigned umsec, unsigned_short time,
      unsigned_short dose, double sense, double charge, int* result));
  STDMETHOD(DoseServer(unsigned in, unsigned sinc, unsigned div,
      unsigned gain, unsigned cond, unsigned pol, unsigned test,
      unsigned mode, unsigned umsec, unsigned_short time,
      unsigned_short dose, double sense, double charge, int stop,
      int* result));
};

#endif //IntServImplH

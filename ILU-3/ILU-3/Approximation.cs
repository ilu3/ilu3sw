﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ILU_3
{
    /// <summary>
    /// Аппроксимация выборки полиномом методом наименьших квадратов
    /// </summary>
    class Approximation
    {
        /// <summary>
        /// Исключение грубых погрешностей
        /// </summary>
        /// <param name="exc_val_current">Набор чисел, из которых необходимо исключить грубые погрешности, превышающие пороговое отконение от среднего этой выборки</param>
        /// <returns>Набор чисел без грубых погрешностей</returns>
        static public List<double> ExceptionOfGrossErrors(List<double> exc_val_current)
        {
            double Mx = 0, s = 0, s2 = 0;
            for (int i = 0; i < exc_val_current.Count; i++)
            {
                Mx += exc_val_current[i];
            }
            Mx = Mx / exc_val_current.Count;//Математическое ожидание

            for (int i = 0; i < exc_val_current.Count; i++)
            {
                s2 += (exc_val_current[i] - Mx) * (exc_val_current[i] - Mx);
            }
            s2 = s2 / exc_val_current.Count;//среднеквадратичное отклоение
            s = Math.Sqrt(s2);

            for (int i = 0; i < exc_val_current.Count; i++)
            {
                double t = Math.Abs(exc_val_current[i] - Mx) / s;
                if (t > 1.927) exc_val_current.RemoveAt(i);//1.927 довер. вер-ть
            }
            return exc_val_current;
        }

        /// <summary>
        /// Аппроксимация выборки методом наименьших квадратов
        /// </summary>
        /// <param name="matrix">Входной набор данных, которые необходимо аппроксимировать</param>
        /// <param name="colCount">Количество столбцов матрицы данных</param>
        /// <param name="rowCount">Количество строк матрицы данных</param>
        /// <returns>Массив коэффициентов аппроксимирующего полинома</returns>
        static public double[] Gauss(double[,] matrix, int rowCount, int colCount)
        {
            int i;
            int[] mask = new int[colCount - 1];
            for (i = 0; i < colCount - 1; i++) mask[i] = i;
            if (GaussDirectPass(ref matrix, ref mask, colCount, rowCount))
            {
                double[] answer = GaussReversePass(ref matrix, mask, colCount, rowCount);
                return answer;
            }
            else return null;
        }

        /// <summary>
        /// Прямой ход метода Гаусса, построение диагональной матрицы
        /// </summary>
        /// <param name="matrix">Входной набор данных, которые необходимо аппроксимировать</param>
        /// <param name="mask">Массив номеров столбцов</param>
        /// <param name="colCount">Количество столбцов матрицы данных</param>
        /// <param name="rowCount">Количество строк матрицы данных</param>
        /// <returns>В случае успешного построения диагональной матрицы: true. В случае неудачи: false</returns>
        static private bool GaussDirectPass(ref double[,] matrix, ref int[] mask, int colCount, int rowCount)
        {
            int i, j, k, maxId, tmpInt;
            double maxVal, tempDouble;
            for (i = 0; i < rowCount; i++)
            {
                maxId = i;
                maxVal = matrix[i, i];
                for (j = i + 1; j < colCount - 1; j++)
                    if (Math.Abs(maxVal) < Math.Abs(matrix[i, j]))
                    {
                        maxVal = matrix[i, j];
                        maxId = j;
                    }
                if (maxVal == 0)
                    return false;
                if (i != maxId)
                {
                    for (j = 0; j < rowCount; j++)
                    {
                        tempDouble = matrix[j, i];
                        matrix[j, i] = matrix[j, maxId];
                        matrix[j, maxId] = tempDouble;
                    }
                    tmpInt = mask[i];
                    mask[i] = mask[maxId];
                    mask[maxId] = tmpInt;
                }
                for (j = 0; j < colCount; j++) matrix[i, j] /= maxVal;
                for (j = i + 1; j < rowCount; j++)
                {
                    double tempMn = matrix[j, i];
                    for (k = 0; k < colCount; k++)
                        matrix[j, k] -= matrix[i, k] * tempMn;
                }
            }
            return true;
        }

        /// <summary>
        /// Обратный ход ход метода Гаусса
        /// </summary>
        /// <param name="matrix">Входной набор данных, которые необходимо аппроксимировать</param>
        /// <param name="mask">Массив номеров столбцов</param>
        /// <param name="colCount">Количество столбцов матрицы данных</param>
        /// <param name="rowCount">Количество строк матрицы данных</param>
        /// <returns>Массив коэффициентов аппроксимирующего полинома</returns>
        static private double[] GaussReversePass(ref double[,] matrix, int[] mask, int colCount, int rowCount)
        {
            int i, j, k;
            for (i = rowCount - 1; i >= 0; i--)
                for (j = i - 1; j >= 0; j--)
                {
                    double tempMn = matrix[j, i];
                    for (k = 0; k < colCount; k++)
                        matrix[j, k] -= matrix[i, k] * tempMn;
                }
            double[] answer = new double[rowCount];
            for (i = 0; i < rowCount; i++) answer[mask[i]] = matrix[i, colCount - 1];
            return answer;
        }

        /// <summary>
        /// Создание матрицы коэффициентов системы линейных алгебраических уравнений
        /// </summary>
        /// <param name="xyTable">Входной набор данных, которые необходимо аппроксимировать</param>
        /// <param name="basis">Степень аппроксимирующего полинома + 1</param>
        /// <param name="NumCount">Размерность квадратной матрицы</param>
        /// /// <returns>Матрица коэффициентов системы линейных алгебраических уравнений</returns>
        static public double[,] MakeSystem(double[,] xyTable, int basis, int NumCount)
        {
            double[,] matrix = new double[basis, basis + 1];
            for (int i = 0; i < basis; i++)
            {
                for (int j = 0; j < basis; j++)
                {
                    matrix[i, j] = 0;
                }
            }
            for (int i = 0; i < basis; i++)
            {
                for (int j = 0; j < basis; j++)
                {
                    double sumA = 0, sumB = 0;
                    for (int k = 0; k < NumCount; k++)
                    {
                        sumA += Math.Pow(xyTable[0, k], i) * Math.Pow(xyTable[0, k], j);
                        sumB += xyTable[1, k] * Math.Pow(xyTable[0, k], i);
                    }
                    matrix[i, j] = sumA;
                    matrix[i, basis] = sumB;
                }
            }
            return matrix;
        }
    }
}

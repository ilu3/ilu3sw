﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Reflection;
using System.ComponentModel;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;

namespace SerialPortListener.Serial
{
    /// <summary>
    /// Класс по последовательной передачи данных COM-порта
    /// </summary>
    public class SerialPortManager : IDisposable
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public SerialPortManager()
        {
            // Поиск установленных последовательных портов на оборудовании
            result_koef = new double[2];
            _currentSerialSettings.PortNameCollection = SerialPort.GetPortNames();
            _currentSerialSettings.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(_currentSerialSettings_PropertyChanged);
            
            // Если последовательные порты найден, мы выбираем первый найденный
            if (_currentSerialSettings.PortNameCollection.Length > 0)
                _currentSerialSettings.PortName = "COM12";//_currentSerialSettings.PortNameCollection[0];
        }

        /// <summary>
        /// Деструктор
        /// </summary>
        ~SerialPortManager()
        {
            Dispose(false);
        }


        #region Fields

        private SerialPort _serialPort;
        private SerialSettings _currentSerialSettings = new SerialSettings();
        //private string _latestRecieved = String.Empty;
        private string get_current="-1";
        private string buf_current = "-1";
        //public event EventHandler<SerialDataEventArgs> NewSerialDataRecieved;
        //List<double> val_current = new List<double>();
        /// <summary>
        /// Коэффициенты калибровки тока магнита
        /// </summary>
        public double[] result_koef;
        Thread thread;
        bool potok = true;

        #endregion

        #region Properties
        /// <summary>
        /// Возвращает или задает текущие параметры последовательного порта
        /// </summary>
        public SerialSettings CurrentSerialSettings
        {
            get { return _currentSerialSettings; }
            set { _currentSerialSettings = value; }
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Событие. Выдача нового бод запроса при изменении последовательного порта
        /// </summary>
        void _currentSerialSettings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //Если последовательный порт изменен, новый бод запрос выдается
            if (e.PropertyName.Equals("PortName"))
                UpdateBaudRateCollection();
        }

        void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int dataLength = _serialPort.BytesToRead;
            byte[] data = new byte[dataLength];
            int nbrDataRead = _serialPort.Read(data, 0, dataLength);//считываем данные из порта в переменную data
            if (nbrDataRead == 0)//если не удалось считать данные
                return;
            var regex = new Regex(@"I(\d+\.?\d{3})?" + Regex.Escape("*") + "");//регулярное выражение для поиска значений тока в принятом сообщении
            
            buf_current = Encoding.ASCII.GetString(data);//записываем в буферную строку данные

                if (regex.IsMatch(buf_current))//если в строке обнаружено соответствие регулярному выражению
                {
                    buf_current = regex.Match(buf_current).Groups[1].Value.ToString();//ищет в строке первое вхождение соответствующее регулярному выражению

                    if (buf_current.Length > 4)//дополнительная проверка на кол-во символов
                    {
                        get_current = buf_current;//записали значение тока магнита
                    }
                    else return;
                }
                else return;
                

            
        }

        #endregion

        #region Methods

        /// <summary>
        /// Подключается к обнаруженному последовательному порту с текущими настройками
        /// </summary>
        public void StartListening()
        {
            // Закрыть последовательный порт, если он открыт
            if (_serialPort != null )
            {
                // _serialPort.Close();

                // Установить настройки последовательного порта
                _serialPort = new SerialPort(
                    _currentSerialSettings.PortName,
                    _currentSerialSettings.BaudRate,
                    _currentSerialSettings.Parity,
                    _currentSerialSettings.DataBits,
                    _currentSerialSettings.StopBits);

                // Подписаться на события и открыть последовательный порт для передачи данных
                _serialPort.Open();
                _serialPort.DataReceived += new SerialDataReceivedEventHandler(_serialPort_DataReceived);

                //Запускается поток, отправляющий запросы тока плате управления atmega128
                thread = new Thread(Request);
                thread.Start();
            }
        }

        /// <summary>
        /// Функция потока, отправляющая запрос тока плате упраления каждые 100мсек
        /// </summary>
        public void Request()
        {
            while (potok)
            {
                _serialPort.WriteLine("#$TI*1D<CR>#");//запрос тока
                Thread.Sleep(100);//каждые 100 мсек
            }
        }

        /// <summary>
        /// Установить ток магнита, Ампер
        /// </summary>
        public void SetCurrentMagnet(double current)
        {
            double cur = result_koef[0] + result_koef[1] * current;
            _serialPort.Write("#DAC" + Convert.ToInt32(cur).ToString() + "#");
            //Thread.Sleep(190);
            //_serialPort.Write("#DAC" + Convert.ToInt32(cur).ToString() + "#");
            Console.WriteLine("#DAC" + Convert.ToInt32(cur).ToString() + "#");
        }
        /// <summary>
        /// Установить ток магнита, целочисленное значение 
        /// </summary>
        /// <param name='current'>ток магнита, Ампер</param>
        public void SetNumValueMagnet(double current)
        {
            //отпрвляем для надежности три запроса на установку тока с небольшим интервалом, т.к.
            //если отправить только один запрос, то в случае потери данных установится неправильный ток
            _serialPort.Write("#DAC" + Convert.ToInt32(current).ToString() + "#");
            Thread.Sleep(150);
            _serialPort.Write("#DAC" + Convert.ToInt32(current).ToString() + "#");
            Console.WriteLine("NUM#DAC" + Convert.ToInt32(current).ToString() + "#");
        }

        /// <summary>
        /// Получить ток магнита
        /// <summary>
        /// <returns>ток магнита, Ампер</returns>
        public double GetCurrentMagnet()
        {

            string res = "-1";

            res = get_current;//получим ток

            int index_of_tochki = res.IndexOf(".");
            if (index_of_tochki != -1)
            {
                res = res.Replace(".", ",");//т.к. Convert.ToDouble поддерживает только дробные числа разделенные запятой
            }

            return Convert.ToDouble(res);
            
         
        }
        
        /// <summary>
        /// Закрыть последовательный порт
        /// </summary>
        public void StopListening()
        {
            _serialPort.Close();
        }

        /// <summary>
        /// Получает структуру COMMPROP текущего выбранного устройства, и извлекает свойство dwSettableBaud
        /// </summary>
        private void UpdateBaudRateCollection()
        {
            _serialPort = new SerialPort(_currentSerialSettings.PortName);
            _serialPort.Open();
            object p = _serialPort.BaseStream.GetType().GetField("commProp", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_serialPort.BaseStream);
            Int32 dwSettableBaud = (Int32)p.GetType().GetField("dwSettableBaud", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).GetValue(p);

            _serialPort.Close();
            _currentSerialSettings.UpdateBaudRateCollection(dwSettableBaud);
        }

        /// <summary>
        /// Очистка ресурсов последовательного порта
        /// </summary>
        public void Dispose()
        {
            Dispose(false);
        }
        /// <summary>
        /// Переобпределение функции Dispose
        /// </summary>
        /// <param name='disposing'>true - отписаться от события SerialDataReceivedEventHandler, false - закрыть последовательный порт</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _serialPort.DataReceived -= new SerialDataReceivedEventHandler(_serialPort_DataReceived);
            }
            // Закрытие последовательного порта
            if (_serialPort != null)
            {
                if (_serialPort.IsOpen)
                    _serialPort.Close();

                _serialPort.Dispose();
            }
            thread.Abort();//закрываем поток
            potok=false;
            
        }


        #endregion

    }

    /// <summary>
    /// EventArgs используется для отправки сообщений на последовательный порт
    /// </summary>
    public class SerialDataEventArgs : EventArgs
    {
        public SerialDataEventArgs(byte[] dataInByteArray)
        {
            Data = dataInByteArray;
        }
        /// <summary>
        /// Байтовый массив, содержащий данные из последовательного порта
        /// </summary>
        public byte[] Data;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using SerialPortListener;

using System.Runtime.InteropServices;
using SerialPortListener.Serial;
using System.Windows.Threading;
using System.IO;
using NationalInstruments.Controls;


namespace ILU_3
{

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields
 
        //конфигурационные параметры
        ///Входной сигал (I-ток, U-напряжение)
        public uint in_sig = 1;
        ///Наличие синхронизации (NOSIN, SIN)
        public uint sinc = 1;
        ///Ослабление входного сигнала (1:1, 1:8)
        public uint div = 1;
        ///Усиление PGA
        public uint gain = 7;
        ///Конденсатор: C2 - 2пФ, C220 - 220пФ
        public uint cond = 1;
        ///Полярность
        public uint pol = 0;
        ///Тестировать источник
        public uint test = 0;
        ///Режим измерения (MES_A - амплитуды, MES_D - дозы)
        public uint mode = 0;
        ///US-мксек, MS-мсек
        public uint umsec = 1;
        ///Время интегрирования (мксек или мсек)
        public ushort time = 200;
        ///Требуемая доза (в циклах перезаряда интегратора)
        public ushort dose = 1000;
        ///Чувств. датчика (A/(А*см2) для тока,A/(В*см2) для напряжения)
        public uint sense = 1;
        ///Зарядность ионов
        public uint charge = 1;

        /// <summary>
        /// Логическая переменная разрешающая(true), запрещающая(false) калибровку миллитесламетра
        /// </summary>
        bool milliteslametr_potok = false;

        /// <summary>
        /// Логическая переменная разрешающая(true), запрещающая(false) запросы тока магнита
        /// </summary>
        bool current_potok = false;

        /// <summary>
        /// Логическая переменная разрешающая(true), запрещающая(false) калибровку тока магнита
        /// </summary>
        bool calib_cur_potok = false;

        /// <summary>
        /// Логическая переменная разрешающая(true), запрещающая(false) отображение уровня магнитного поля в элементе управления textBox_tesla
        /// </summary>
        bool magnet_potok = false;

        /// <summary>
        /// Логическая переменная разрешающая(true), запрещающая(false) регистрацию ионного спектра
        /// </summary>
        bool spectr_potok = false;

        /// <summary>
        /// Список значений тока магнита (Ампер), полученный при калибровке
        /// </summary>
        List<double> val_current = new List<double>();

        /// <summary>
        /// Список значений уровня магнитного поля (мТл), полученный при калибровке
        /// </summary>
        List<double> val_magnet = new List<double>();

        /// <summary>
        /// Массив калибровочных коэффициентов для тока магнита
        /// </summary>
        double[] result_koef_cur;

        /// <summary>
        /// Массив калибровочных коэффициентов для получения магнитного поля, используя ток магнита
        /// </summary>
        double[] result_koef_mag;

        /// <summary>
        /// Массив калибровочных коэффициентов для получения тока магнита, используя магнитное поле
        /// </summary>
        double[] result_koef_mag_inv;

        /// <summary>
        /// Текущий ток магнита
        /// </summary>
        double temp_current2 = 0;

        /// <summary>
        /// Массив точек (ток, магнитное поле) для отображения результата калибровки миллитесламетра на графике
        /// </summary>
        Point[] dataXYmag;

        /// <summary>
        /// Текущий уровень магнитного поля (мТл) во время калибровки
        /// </summary>
        double res_mag_f = 0;

        /// <summary>
        /// Минимальный ток (Ампер) для калибровки миллитесламетра
        /// </summary>
        int minC = 0;

        /// <summary>
        /// Максимальный ток (Ампер) для калибровки миллитесламетра
        /// </summary>
        int maxC = 0;

        /// <summary>
        /// Минимальное число (целочисленное значение от 0 до 65535) для калибровки тока магнита
        /// </summary>
        int minI = 0;

        /// <summary>
        /// Максимальное число (целочисленное значение от 0 до 65535) для калибровки тока магнита
        /// </summary>
        int maxI = 0;

        /// <summary>
        /// Минимальное значение тока магнита при регистрации ионного спетра
        /// </summary>
        double min_c = 40;
        /// <summary>
        /// Максимального значение тока магнита при регистрации ионного спетра
        /// </summary>
        double max_c = 100;
        /// <summary>
        /// Текущее значение тока магнита при регистрации ионного спетра
        /// </summary>
        double temp_c = 40;

        /// <summary>
        /// Поток регистрации ионного спектра
        /// </summary>
        Thread spectr_thread;

        /// <summary>
        /// Поток отображения текущего уровня магнитного поля
        /// </summary>
        Thread textbox_magnet;

        /// <summary>
        /// Список хранит токи регистрируемые интегратором дозы 
        /// </summary>
        List<double> data_y;

        /// <summary>
        /// Список хранит токи магнита при регистрации ионного спектра
        /// </summary>
        List<double> data_x;

        /// <summary>
        /// Поток калибровки тока магнита
        /// </summary>
        Thread calib_cur_thread;

        /// <summary>
        /// Поток калибровки миллитесламетра
        /// </summary>
        Thread millitealametr_thread;

        /// <summary>
        /// Объект класса последовательной передачи данных COM-порта
        /// </summary>
        SerialPortManager _spManager;

        /// <summary>
        /// Интерфейс сервера автоматизации "Интегратор дозы"
        /// </summary>
        public IntegratorServer.IntServ int_srv = new IntegratorServer.IntServ();

        #endregion

        [DllImport("PriborFunc.dll")]///Импорт функции подключения к ip-камере и захвата изображения
        public static extern void CameraConnect();
        [DllImport("PriborFunc.dll")]///Импорт функции распознавания показаний милитесламетра
        public static extern double GetMagnetFild();

        /// <summary>
        /// Конструктор
        /// </summary>
        public MainWindow()
        {
            /// <summary>
            /// Происходит считывание коэффицентов калибровки тока из файла "cur_koef.txt", считывание коэффицентов 
            /// калибровки миллитесламетра из файла "mag_koef.txt", считывания из файла "mag_koef_inv.txt" коэффицентов 
            /// калибровки миллитесламетра для получения тока магнита, используя магнитное поле.
            /// 
            /// Инициализация объектов SerialPortManager, SerialSettings
            /// 
            /// Подключение к интегратору дозы и плате управления током
            /// 
            /// Запуск потока для отображения текущей величины тока магнита
            /// 
            /// Запуск потока для отображения индукции магнитного поля, используя коэффиценты
            /// калибровки миллитесламетра, загруженные из файла "mag_koef.txt"
            /// 
            /// Подключение к камере и старт потока для захвата изображения с камеры
            /// </summary>
            
            StreamReader wt = new StreamReader("cur_koef.txt");
            //Реализует System.IO.TextReader для считывания коэффицентов калибровки миллитесламетра из файла "cur_koef.txt"
            StreamReader wt2 = new StreamReader("mag_koef.txt");
            // Реализует System.IO.TextReader для считывания из файла "cur_koef.txt" коэффицентов калибровки миллитесламетра для получения тока магнита, используя магнитное поле 
            StreamReader wt3 = new StreamReader("mag_koef_inv.txt");
            result_koef_cur = new double[2];
            result_koef_mag_inv = new double[4];
            result_koef_mag = new double[4];
            for (int i = 0; i < 2; i++)
            {
                string line = wt.ReadLine();   
                result_koef_cur[i]=Convert.ToDouble(line);
            }
            wt.Close();

            for (int i = 0; i < 4; i++)
            {
                string line = wt2.ReadLine();
                result_koef_mag[i] = Convert.ToDouble(line);
            }
            wt2.Close();

            for (int i = 0; i < 4; i++)
            {
                string line = wt3.ReadLine();
                result_koef_mag_inv[i] = Convert.ToDouble(line);
            }
            wt3.Close();

            _spManager = new SerialPortManager();
            // Объект класса, содержащего свойства, связанные с последовательным портом
            SerialSettings mySerialSettings = _spManager.CurrentSerialSettings;
            _spManager.result_koef[0] = result_koef_cur[0];
            _spManager.result_koef[1] = result_koef_cur[1];

            InitializeComponent();
            powerButton1.Value = false;
            powerButton2.Value = false;
            unsafe
            {
                //Подключить итегратор дозы 
                int op_res; int_srv.OpenComServer(out op_res);
                if (op_res != 0) MessageBox.Show("Интегратор дозы подключен","ИЛУ-3",MessageBoxButton.OK,MessageBoxImage.Exclamation);
                if (op_res == 0) MessageBox.Show("Интегратор дозы не подключен","ИЛУ-3",MessageBoxButton.OK,MessageBoxImage.Error);
            }
            try
            {
                if (_spManager.CurrentSerialSettings.PortName != "")
                {
                    //подключиться к плате управления током
                    _spManager.StartListening();

                    Thread current_thread = new Thread(new ThreadStart(CurrentThreadFunction));//стартуем поток отображения тока магнита
                    current_potok = true;

                    current_thread.Start();

                    magnet_potok = true;//стартуем поток отображения уровня магнитного поля, используя ток и коэффиценты калибровки
                    textbox_magnet = new Thread(new ThreadStart(MagnetThreadFunction));
                    textbox_magnet.Start();
                }
                else
                    MessageBox.Show("Подключите usb-кабель измерителя тока и перезапустите приложение","ИЛУ-3",MessageBoxButton.OK,MessageBoxImage.Error);
            }
            catch
            {
                MessageBox.Show("Подключите usb-кабель измерителя тока и перезапустите приложение","ИЛУ-3",MessageBoxButton.OK,MessageBoxImage.Error);
            }
            try
            {
                //CameraConnect();
            }
            catch
            {
                MessageBox.Show("Подключите ethernet-кабель камеры", "ИЛУ-3", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #region Invoke Methods
        
        /// <summary>
        ///Делегат, содержащий метод InvokeTextBoxMagnet, который требуется вызывать в контексте потока элемента управления
        /// </summary>
        public delegate void InvokeDelegateTextBoxMagnet();

        /// <summary>
        ///Делегат, содержащий метод InvokeDrawSpectr, который требуется вызывать в контексте потока элемента управления
        /// </summary>
        public delegate void InvokeDelegateDrawSpectr();

        /// <summary>
        ///Делегат, содержащий метод InvokeDrawCalibMag, который требуется вызывать в контексте потока элемента управления
        /// </summary>
        public delegate void InvokeDelegateDrawCalibMag();

        /// <summary>
        ///Делегат, содержащий метод InvokeSetCurrent, который требуется вызывать в контексте потока элемента управления
        /// </summary>
        public delegate void InvokeDelegateSetCurrent();

        /// <summary>
        ///Делегат, содержащий метод LabelGetCurrent, который требуется вызывать в контексте потока элемента управления
        /// </summary>
        public delegate void InvokeDelegateLabelGetCurrent();

        /// <summary>
        ///Делегат, содержащий метод LabelSetMagnet, который требуется вызывать в контексте потока элемента управления
        /// </summary>
        public delegate void InvokeDelegateLabelSetMagnet();

        /// <summary>
        ///Делегат, содержащий метод InvokeSetMagnet, который требуется вызывать в контексте потока элемента управления
        /// </summary>
        public delegate void InvokeDelegateSetMagnet();

        /// <summary>
        ///Делегат, содержащий метод InvokeLedButtonOff, который требуется вызывать в контексте потока элемента управления
        /// </summary>
        public delegate void InvokeDelegateLedButtonOff();

        /// <summary>
        ///Делегат, содержащий метод InvokeGetMagnet, который требуется вызывать в контексте потока элемента управления
        /// </summary>
        public delegate void InvokeDelegateGetMagnet();

        /// <summary>
        /// Отобразить уровень магнитного поля (мТл) в элементе управления textBox_tesla во время калибровки миллитесламетра (калибровочные
        /// коэффициенты не используются, отображается результат распознавания показаний миллитесламетра)
        /// </summary>
        public void InvokeTextBoxMagnet()
        {
            if(res_mag_f!=-1) 
                textBox_tesla.Text = Math.Round(res_mag_f).ToString() + "Тл";
        }

        /// <summary>
        /// Отобразить на элементе управления writableGraph1 график калибровки миллитесламетра (ось X: Ампер, ось Y: мТл)
        /// </summary>
        public void InvokeDrawCalibMag()
        {
            writableGraph1.DataSource = dataXYmag;
        }

        /// <summary>
        /// Отобразить на элементе управления label_set_current ток магнита, Ампер
        /// </summary>
        public void InvokeLabelSetCurrent()
        {
            label_set_current.Content = Math.Round(knobDouble_cur.Value, 1) + " A";//когда крутим ручку для установки тока магнита, подпись меняется
        }

        /// <summary>
        /// Отобразить на элементах управления label_get_current и meterDouble_get_current ток магнита, Ампер
        /// </summary>
        public void InvokeLabelGetCurrent()
        {
            try
            {
                meterDouble_get_current.Value = _spManager.GetCurrentMagnet();
            }
            catch
            {
                MessageBox.Show("Подключите usb-кабель измерителя тока и нажмите ОК", "ИЛУ-3", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            temp_current2 = meterDouble_get_current.Value;
            label_get_current.Content = Math.Round(meterDouble_get_current.Value, 1).ToString() + " A";
            
        }

        /// <summary>
        /// Установить ток магнита
        /// </summary>
        public void InvokeSetCurrent()
        {
            temp_current2 = knobDouble_cur.Value;
            double cur = knobDouble_cur.Value;//персчитаем ток выбранный пользователем с помощью
            //элемента управления knobDouble_cur и отправляем запрос установки тока плате управления током
            _spManager.SetCurrentMagnet(cur);
        }

        /// <summary>
        /// Отобразить на элементе управления label_mag уровень магнитного поля 
        /// </summary>
        public void InvokeLabelSetMagnet()
        {
            label_mag.Content = Math.Round(knobDouble_tesla.Value, 1) + " мТл";//когда крутим ручку подпись меняется 
        }

        /// <summary>
        /// Установить магнитное поле с помощью элемента управления knobDouble_tesla, используя коэффиценты калибровки
        /// </summary>
        public void InvokeSetMagnet()
        {
            double t_cur = result_koef_mag[3] * Math.Pow(knobDouble_tesla.Value, 3) + result_koef_mag[2] * Math.Pow(knobDouble_tesla.Value, 2)
                + result_koef_mag[1] * knobDouble_tesla.Value + result_koef_mag[0];//
            //double cur = result_koef_cur[0] + result_koef_cur[1] * t_cur;
            _spManager.SetCurrentMagnet(t_cur);
        }

        /// <summary>
        /// Пересчитать магнитное поле (мТл) через ток (А), используя калибровочные коэффиценты и отобразить в элементе управления textBox_tesla
        /// </summary>
        public void InvokeGetMagnet()
        {
            double mag_f = result_koef_mag_inv[3] * Math.Pow(temp_current2, 3) + result_koef_mag_inv[2] * temp_current2 * temp_current2 + result_koef_mag_inv[1] * temp_current2 + result_koef_mag_inv[0];
            textBox_tesla.Text = Math.Round(mag_f).ToString() + " мТл";
        }

        /// <summary>
        /// Отключить элементы управления светодиоды и кнопки запуска калибровки миллитесламетра и тока магнита
        /// </summary>
        public void InvokeLedButtonOff()
        {
            led1.Value = false;
            powerButton1.Value = false;
            lED2.Value = false;
            powerButton2.Value = false;
        }

        /// <summary>
        /// Метод для вывода полученных измерений в элемент управления writableGraph1
        /// </summary>
        public void InvokeDrawSpectr()
        {
            double min_h = result_koef_mag_inv[3] * Math.Pow(min_c, 3) + result_koef_mag_inv[2] * min_c * min_c + result_koef_mag_inv[1] * min_c + result_koef_mag_inv[0];
            double temp_h = result_koef_mag_inv[3] * Math.Pow(temp_c, 3) + result_koef_mag_inv[2] * temp_c * temp_c + result_koef_mag_inv[1] * temp_c + result_koef_mag_inv[0];


            //((AxisDouble)writableGraph1.Axes[0]).Range = new Range<double>(min_c, temp_c);
            ((AxisDouble)writableGraph1.Axes[1]).Range = new Range<double>(min_h, temp_h);

            min_h = min_h / 1000 * 10000;//перевод из миллитесла в эрстэд
            temp_h = temp_h / 1000 * 10000;//перевод из миллитесла в эрстэд
            double min_aem = min_h * min_h / 46.0 * 1.126E-4;
            double temp_aem = temp_h * temp_h / 46.0 * 1.126E-4;
            ((AxisDouble)writableGraph1.Axes[2]).Range = new Range<double>(min_aem, temp_aem);

            Point[] data = new Point[data_x.Count];
            for (int i = 0; i < data_x.Count; i++)
            {
                data[i] = new Point(data_x[i], data_y[i]);
            }
            writableGraph1.DataSource = data;
        }

#endregion

        #region Thread Methods
        /// <summary>
        /// Калибровка миллитесламетра
        /// </summary>
        private void MillitealametrThreadFunction()
        {
            List<double> currents = new List<double>();//ток
            List<double> magnets = new List<double>();//магнитное поле
            try
            {
                while (milliteslametr_potok)
                {
                    unsafe
                    {
                        double[,] xyTable, matrix;

                        int row = 0; int step = 4;
                        _spManager.SetCurrentMagnet(minC);//устанавливаем начальное значение
                        Thread.Sleep(6000);
                        double cur_valid = _spManager.GetCurrentMagnet();//проверим установился ли начальный ток
                        if (cur_valid < minC - 2 && cur_valid > minC + 2)//проверим установился ли начальный ток
                            Thread.Sleep(3000);

                        for (double cur = minC; cur < maxC; cur += step)
                        {
                            _spManager.SetCurrentMagnet(cur);
                            Thread.Sleep(6000);
                            int count = 0;

                            while (count < 5)
                            {
                                double current = _spManager.GetCurrentMagnet();
                                if (row > 0 && current < 0.8 * currents[currents.Count - 1])
                                {
                                    cur += 0.5;
                                    _spManager.SetCurrentMagnet(cur);
                                    Thread.Sleep(4000);
                                    current = _spManager.GetCurrentMagnet();
                                }
                                if (current != -1)
                                {
                                    val_current.Add(current);
                                    count++;
                                    Thread.Sleep(50);
                                }

                            }
                            val_current = Approximation.ExceptionOfGrossErrors(val_current);
                            double Mx_current = 0;
                            for (int i = 0; i < val_current.Count; i++)
                            {
                                Mx_current += val_current[i];
                            }
                            Mx_current = Mx_current / val_current.Count;
                            currents.Add(Mx_current);
                            val_current.Clear();

                            int count2 = 0;
                            int error = 0; //счетчик количества ошибок;
                            while (true)
                            {
                                if (count2 > 5) break;
                                double magnet = GetMagnetFild();
                                if (magnet != -1)
                                {
                                    if (row > 0)
                                    {
                                        int last = magnets.Count - 1;
                                        if (magnet > magnets[last] && magnet > 50 && magnet < 1000)
                                        {
                                            val_magnet.Add(magnet);
                                        }
                                        if (magnet < magnets[last] && magnet > 50 && magnet < 1000)
                                        {
                                            val_magnet.Add(magnets[last]);
                                        }
                                    }
                                    else
                                    {
                                        val_magnet.Add(magnet);
                                    }
                                    count2++;
                                }
                                else
                                {
                                    error++;
                                    Thread.Sleep(10);
                                    if (error > 10) MessageBox.Show("Не удалось распознать изображение с камеры", "Ошибка");
                                }
                            }

                            double Mx_magnet = 0;
                            for (int i = 0; i < val_magnet.Count; i++)
                            {
                                Mx_magnet += val_magnet[i];
                            }
                            Mx_magnet = Mx_magnet / val_magnet.Count;
                            magnets.Add(Mx_magnet);
                            res_mag_f = Mx_magnet;
                            Dispatcher.BeginInvoke(new InvokeDelegateTextBoxMagnet(InvokeTextBoxMagnet));
                            val_magnet.Clear();
                            row++;
                        }
                        if (magnets.Count == 0) MessageBox.Show("Не удалось выполнить калибровку магнитного поля!", "Ошибка");
                        //коэффициенты(зная поле, получим ток)
                        xyTable = new double[2, row];

                        for (int i = 0; i < row; i++)
                        {
                            xyTable[0, i] = magnets[i];//по Х ток в амперах
                            xyTable[1, i] = currents[i];//по Y ток в числах от 1 до 65535
                        }

                        int basis = Convert.ToInt32(3) + 1;//степень полинома = 3

                        matrix = Approximation.MakeSystem(xyTable, basis, row);

                        result_koef_mag = Approximation.Gauss(matrix, basis, basis + 1);
                        if (result_koef_mag == null)
                        {
                            MessageBox.Show("Не удалось вычислить коэффициенты калибровки магнитного поля!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }


                        for (int i = 0; i < row; i++)
                        {
                            xyTable[0, i] = currents[i];//по Х ток в амперах
                            xyTable[1, i] = magnets[i];//по Y поле
                        }

                        matrix = Approximation.MakeSystem(xyTable, basis, row);

                        result_koef_mag_inv = Approximation.Gauss(matrix, basis, basis + 1);

                        if (result_koef_mag_inv == null)
                        {
                            MessageBox.Show("Не удалось вычислить коэффициенты калибровки магнитного поля!", "Ошибка");
                            return;
                        }

                        dataXYmag = new Point[row];

                        for (int i = 0; i < row; i++)
                        {
                            dataXYmag[i] = new Point(xyTable[0, i], xyTable[1, i]);
                        }

                        Dispatcher.BeginInvoke(new InvokeDelegateDrawCalibMag(InvokeDrawCalibMag));
                        StreamWriter wt = new StreamWriter("mag_koef.txt");
                        for (int i = 0; i < 4; i++)
                        {
                            wt.Write(result_koef_mag[i] + wt.NewLine);
                        }
                        wt.Close();
                        StreamWriter wt2 = new StreamWriter("mag_koef_inv.txt");
                        for (int i = 0; i < 4; i++)
                        {
                            wt2.Write(result_koef_mag_inv[i] + wt.NewLine);
                        }
                        wt2.Close();

                        magnet_potok = true;
                        textbox_magnet = new Thread(new ThreadStart(MagnetThreadFunction));
                        textbox_magnet.Start();

                        MessageBox.Show("Калибровка магнитного поля завершена!");
                        milliteslametr_potok = false;
                        lED2.Value = false;
                        powerButton2.Value = false;

                    }
                }

            }
            catch (Exception ex)
            {
                // log errors
            }
        }

        /// <summary>
        /// Функция потока получения текущего тока магнита
        /// </summary>
        private void CurrentThreadFunction()
        {
            while (current_potok)
            {
                Thread.Sleep(101);
                Dispatcher.BeginInvoke(new InvokeDelegateLabelGetCurrent(InvokeLabelGetCurrent));
            }
        }

        /// <summary>
        /// Функция потока получения текущего уровня магнитного поля
        /// </summary>
        private void MagnetThreadFunction()
        {
            while (magnet_potok)
            {
                Dispatcher.BeginInvoke(new InvokeDelegateGetMagnet(InvokeGetMagnet));
                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// Функция потока калибровки тока магнита
        /// </summary>
        private void CalibCurThreadFunction()//
        {
            if (calib_cur_potok)
            {
                CalibrateCurrentMagnetForm();
            }
        }

        /// <summary>
        /// Функция потока для получения ионного спектра
        /// </summary>
        public void Processing()
        {

            data_x = new List<double>();//по оси x текущий ток магнита
            data_y = new List<double>();//по оси y ток в цилиндре Фарадея для данного тока магнита
            double u, i, skv, fdoza, j_mid, j_imp, q;
            uint adc_data, d;
            temp_c = min_c;
            double delta_c = 0.3;

            _spManager.SetCurrentMagnet(temp_c);
            Thread.Sleep(2000);
            for (double k = 0; k < (max_c - min_c) / delta_c; k++)//проводим измерения тока пучка
            {
                _spManager.SetCurrentMagnet(temp_c);
                /*if (k > 0 && _spManager.GetCurrentMagnet() < 0.8 * data_x[data_x.Count - 1])
                {
                    temp_c += 0.1;
                    _spManager.SetCurrentMagnet(temp_c);
                    Thread.Sleep(3000);
                }*/
                Thread.Sleep(200);

                //valid_cur = _spManager.GetCurrentMagnet();
                int_srv.MeasureServer(
                    in_sig,
                    sinc,
                    div,
                    gain,
                    cond,
                    pol,
                    test,
                    mode,
                    umsec,
                    time,
                    dose,
                    sense,
                    charge,
                    out adc_data,
                    out u,
                    out i,
                    out d,
                    out skv,
                    out fdoza,
                    out j_mid,
                    out j_imp,
                    out q);


                data_y.Add(i);
                data_x.Add(temp_c);
                temp_c += delta_c;

                Dispatcher.BeginInvoke(new InvokeDelegateDrawSpectr(InvokeDrawSpectr));
            }
            Dispatcher.BeginInvoke(new InvokeDelegateDrawSpectr(InvokeDrawSpectr));
        }

        #endregion

        #region Methods
        /// <summary>
        /// Калибровка тока магнита
        /// </summary>
        public void CalibrateCurrentMagnetForm()
        {
            double[,] xyTable, matrix;

            List<double> currents = new List<double>();
            List<double> numbers = new List<double>();

            int row = 0; int step = 1000;
            _spManager.SetNumValueMagnet(minI);
            Thread.Sleep(2000);

            for (int cur = Convert.ToInt32(minI); cur < Convert.ToInt32(maxI); cur += step)
            {
                //tankDouble1.Value += 4.7;
                _spManager.SetNumValueMagnet(cur);
                numbers.Add(cur);
                Thread.Sleep(1000);
                //tank1_val += 6;
                int count = 0;

                while (count < 10)
                {
                    double current = _spManager.GetCurrentMagnet();
                    if (current != -1)
                    {
                        val_current.Add(current);
                        count++;
                        Thread.Sleep(50);
                    }
                }
                val_current=Approximation.ExceptionOfGrossErrors(val_current);
                double Mx_current = 0;
                for (int i = 0; i < val_current.Count; i++)
                {
                    Mx_current += val_current[i];
                }
                Mx_current = Mx_current / val_current.Count;
                currents.Add(Mx_current);
                val_current.Clear();
                row++;
            }

            xyTable = new double[2, row];
            try
            {
                for (int i = 0; i < row; i++)
                {
                    xyTable[0, i] = currents[i];//по Х ток в амперах
                    xyTable[1, i] = numbers[i];//по Y ток в числах от 1 до 65535
                }
            }
            catch
            {
                return;
            }
            int basis = Convert.ToInt32(1) + 1;//степень полинома = 3

            matrix = Approximation.MakeSystem(xyTable, basis, row);

            result_koef_cur = Approximation.Gauss(matrix, basis, basis + 1);
            if (result_koef_cur == null)
            {
                MessageBox.Show("Не удалось вычислить коэффициенты калибровки тока магнита!", "Ошибка");
                return;
            }

            StreamWriter wt = new StreamWriter("cur_koef.txt");
            for (int i = 0; i < 2; i++)
            {
                wt.Write(result_koef_cur[i]+wt.NewLine);
            }
            wt.Close();
            MessageBox.Show("Калибровка тока завершена!");
            calib_cur_potok = false;
            Dispatcher.BeginInvoke(new InvokeDelegateLedButtonOff(InvokeLedButtonOff));

        }

        /// <summary>
        /// Обновить конфигурационные параметры интегратора дозы
        /// </summary>
        public void UpdateConfig()
        {

            if (comboBox_in.Text == "Напряжение") in_sig = 1;
            if (comboBox_in.Text == "Ток") in_sig = 0;

            if (comboBox_sinc.Text == "Да") sinc = 1;
            if (comboBox_sinc.Text == "Нет") sinc = 0;

            if (comboBox_div.Text == "1:1") div = 0;
            if (comboBox_div.Text == "1:8") div = 1;

            if (comboBox_gain.Text == "x1") gain = 0;
            if (comboBox_gain.Text == "x2") gain = 1;
            if (comboBox_gain.Text == "x4") gain = 2;
            if (comboBox_gain.Text == "x5") gain = 3;
            if (comboBox_gain.Text == "x8") gain = 4;
            if (comboBox_gain.Text == "x10") gain = 5;
            if (comboBox_gain.Text == "x16") gain = 6;
            if (comboBox_gain.Text == "x32") gain = 7;

            if (comboBox_cond.Text == "2пФ") cond = 0;
            if (comboBox_cond.Text == "220пФ") cond = 1;

            if (comboBox_pol.Text == "Положительная") pol = 0;
            if (comboBox_pol.Text == "Отрицательная") pol = 1;

            if (comboBox_test.Text == "Тестировать") test = 0;
            if (comboBox_test.Text == "Не тестировать") test = 1;

            if (comboBox_mode.Text == "Амплитуда") mode = 0;
            if (comboBox_mode.Text == "Доза") mode = 1;

            if (comboBox_umsec.Text == "мсек") umsec = 1;
            if (comboBox_umsec.Text == "мксек") umsec = 0;



        }

        #endregion

        #region Event Handlers
        
        /// <summary>
        /// Обработчик изменения значения элемента управления knobDouble_cur. Вызывает метод
        /// для отображения тока магнита на элементе управления label_cur
        /// </summary>
        private void knobDouble1_ValueChanged_1(object sender, NationalInstruments.Controls.ValueChangedEventArgs<double> e)
        {
            Dispatcher.BeginInvoke(new InvokeDelegateSetCurrent(InvokeLabelSetCurrent));
        }

        /// <summary>
        /// Вызывает метод InvokeSetCurrent для установки тока магнита, когда пользователь
        /// отпускает левую кнопку мыши на элементе управления knobDouble_cur
        /// </summary>
        private void knobDouble1_MouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            Dispatcher.BeginInvoke(new InvokeDelegateSetCurrent(InvokeSetCurrent));
        }

        /// <summary>
        /// Обработчик изменения значения элемента управления knobDouble_tesla. Вызывает метод
        /// для отображения тока магнита на элементе управления label_tesla
        /// </summary>
        private void knobDouble2_ValueChanged(object sender, NationalInstruments.Controls.ValueChangedEventArgs<double> e)
        {
            Dispatcher.BeginInvoke(new InvokeDelegateLabelSetMagnet(InvokeLabelSetMagnet));
        }

        /// <summary>
        /// Вызывает метод InvokeSetMagnet для установки магнитного поля, когда пользователь
        /// отпускает левую кнопку мыши на элементе управления knobDouble_tesla
        /// </summary>
        private void knobDouble2_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Dispatcher.BeginInvoke(new InvokeDelegateSetMagnet(InvokeSetMagnet));
        }

        /// <summary>
        /// Обработчик нажатия кнопки powerButton2, запускает поток калибровки
        /// миллитесламетра, загорается/выключается светодиод (элемент управления lED2)
        /// </summary>
        private void powerButton2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<bool> e)
        {
            if (!powerButton2.Value)
            {
                milliteslametr_potok = false;
                millitealametr_thread.Abort();
                
                lED2.Value = false;
            }
            if (powerButton2.Value)
            {
                magnet_potok = false;
                textbox_magnet.Abort();
                lED2.Value = true;
                //CameraConnect();
                milliteslametr_potok = true;
                minC = Convert.ToInt32(minCur.Value);
                maxC = Convert.ToInt32(maxCur.Value);
                millitealametr_thread = new Thread(new ThreadStart(MillitealametrThreadFunction));
                millitealametr_thread.Start();
            }
        }

        /// <summary>
        /// Обработчик нажатия кнопки powerButton1, запускает/останавливает поток калибровки
        /// миллитесламетра, загорается/выключается светодиод (элемент управления led1)
        /// </summary>
        private void powerButton1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<bool> e)
        {

            if (!powerButton1.Value)
            {
                calib_cur_thread.Abort();
                calib_cur_potok = false;
                led1.Value = false;
            }
            if (powerButton1.Value)
            {
                led1.Value = true;
                minI = (int)minInt.Value;
                maxI = (int)maxInt.Value;
                calib_cur_potok = true;
                calib_cur_thread = new Thread(new ThreadStart(CalibCurThreadFunction));
                calib_cur_thread.Start();
            }
        }

        /// <summary>
        /// Обработчик нажатия кнопки "Измерение", выполняет единичное измерение ионного тока
        /// </summary>
        private void button_measure_Click_1(object sender, RoutedEventArgs e)
        {

            UpdateConfig();

            double u, i, skv, fdoza, j_mid, j_imp, q;
            uint adc_data, d;
            int_srv.MeasureServer(
                in_sig,
                sinc,
                div,
                gain,
                cond,
                pol,
                test,
                mode,
                umsec,
                time,
                dose,
                sense,
                charge,
                out adc_data,
                out u,
                out i,
                out d,
                out skv,
                out fdoza,
                out j_mid,
                out j_imp,
                out q);
            
            textBox2.Text = adc_data.ToString();
            textBox3.Text = u.ToString();
            textBox4.Text = i.ToString();
            textBox5.Text = d.ToString();
            textBox6.Text = skv.ToString();
            textBox7.Text = fdoza.ToString();
            textBox8.Text = j_mid.ToString();
            textBox9.Text = j_imp.ToString();
            textBox10.Text = q.ToString();
             
        }

        /// <summary>
        /// Обработчик нажатия кнопки "Подключить", выполняет подключение к интегратору дозы через COM-порт
        /// </summary>
        private void button_connect_Click(object sender, RoutedEventArgs e)
        {
            unsafe
            {
                int op_res; int_srv.OpenComServer(out op_res);
                if(op_res!=0) MessageBox.Show("Интегратор дозы подключен");
                else MessageBox.Show("Не удалось подключить интегратор дозы");
            }
        }

        /// <summary>
        /// Обработчик нажатия кнопки "Спектр", запускает поток измерения ионного спектра
        /// </summary>
        private void button_spectr_Click(object sender, RoutedEventArgs e)
        {
            min_c = numericTextBox_min_c.Value;
            max_c = numericTextBox_max_c.Value;
            UpdateConfig();
            spectr_potok = true;
            spectr_thread = new Thread(new ThreadStart(Processing));
            spectr_thread.Start();
        }
        #endregion
    }
}

//**************************************************************
//�����������:      ������� ���� ����, �. ������ ��������
//������:	        ��� ���-3
//������ �������:   1.0
//������������:     ANSI C++ 98
//������:   		21.04.2014
//�������:   		27.09.2015
//**************************************************************/

/** \author ��������� �.�.*/
/** \file Milliteslametr.h*/
/** \brief ���������� ������� ��� ���������� �����������.*/

#include "Filter.h"

void Filter::cvShiftDFT(CvArr * src_arr, CvArr * dst_arr )
{
    CvMat * tmp = NULL;
    CvMat q1stub, q2stub;
    CvMat q3stub, q4stub;
    CvMat d1stub, d2stub;
    CvMat d3stub, d4stub;
    CvMat * q1, * q2, * q3, * q4;
    CvMat * d1, * d2, * d3, * d4;

    CvSize size = cvGetSize(src_arr);
    CvSize dst_size = cvGetSize(dst_arr);
    int cx, cy;

    if(dst_size.width != size.width || 
       dst_size.height != size.height){
        cvError( CV_StsUnmatchedSizes, 
		   "cvShiftDFT", "Source and Destination arrays must have equal sizes", 
		   __FILE__, __LINE__ );   
    }

    if(src_arr==dst_arr){
        tmp = cvCreateMat(size.height/2, size.width/2, cvGetElemType(src_arr));
    }
    
    cx = size.width/2;
    cy = size.height/2; // image center

    q1 = cvGetSubRect( src_arr, &q1stub, cvRect(0,0,cx, cy) );
    q2 = cvGetSubRect( src_arr, &q2stub, cvRect(cx,0,cx,cy) );
    q3 = cvGetSubRect( src_arr, &q3stub, cvRect(cx,cy,cx,cy) );
    q4 = cvGetSubRect( src_arr, &q4stub, cvRect(0,cy,cx,cy) );
    d1 = cvGetSubRect( src_arr, &d1stub, cvRect(0,0,cx,cy) );
    d2 = cvGetSubRect( src_arr, &d2stub, cvRect(cx,0,cx,cy) );
    d3 = cvGetSubRect( src_arr, &d3stub, cvRect(cx,cy,cx,cy) );
    d4 = cvGetSubRect( src_arr, &d4stub, cvRect(0,cy,cx,cy) );

    if(src_arr!=dst_arr){
        if( !CV_ARE_TYPES_EQ( q1, d1 )){
            cvError( CV_StsUnmatchedFormats, 
			"cvShiftDFT", "Source and Destination arrays must have the same format",
			__FILE__, __LINE__ ); 
        }
        cvCopy(q3, d1, 0);
        cvCopy(q4, d2, 0);
        cvCopy(q1, d3, 0);
        cvCopy(q2, d4, 0);
    }
    else{
        cvCopy(q3, tmp, 0);
        cvCopy(q1, q3, 0);
        cvCopy(tmp, q1, 0);
        cvCopy(q4, tmp, 0);
        cvCopy(q2, q4, 0);
        cvCopy(tmp, q2, 0);
		
		cvReleaseMat(&tmp);
    }

}

void Filter::create_butterworth_homomorphic_filter(CvMat* dft_Filter, int D, int n, float upper, float lower)
{
	CvMat* single = cvCreateMat(dft_Filter->rows, dft_Filter->cols, CV_64FC1 ); 
	
	CvPoint centre = cvPoint(dft_Filter->rows / 2, dft_Filter->cols / 2);
	double radius;
	
	// details based on Gonzalez/Woods 3rd Edition, p293 w/ guidance from
	
	for(int i = 0; i < dft_Filter->rows; i++)
	{
		for(int j = 0; j < dft_Filter->cols; j++)
		{
			radius = (double) sqrt(pow((i - centre.x), 2.0) + pow((double) (j - centre.y), 2.0));
			CV_MAT_ELEM(*single, double, i, j) = 
			((upper - lower) * ( 1 / (1 + pow((double) (D /  radius), (double) (2 * n))))) + lower;
		}	
	}

	// N.B. perhaps need to check that all elements of single are within a reasonable range
	// - NOT DONE

	cvMerge(single, single, NULL, NULL, dft_Filter);
	
	cvReleaseMat(&single);	
}
	

Mat Filter::homomorphic_filter(Mat &in_img)
{
  Mat out_img;
  IplImage* img = NULL;      // image object	
  CvCapture* capture = NULL; // capture object
	
  IplImage* dft_spec_mag = NULL;	
	
  int radiusD = 10;				// radius of band pass filter parameter	
  int order = 2;	            // order of band pass filter parameter
	
  int high_h_v_TB = 101;	    // trackbar control value upper 
  int low_h_v_TB = 30; 			// trackbar control value lower
	
  bool doHistEq = false;		// flag for hist. eq. output	

		
	  img = cvCloneImage(&(IplImage)in_img);//cvLoadImage( "IMG_0295.JPG", CV_LOAD_IMAGE_UNCHANGED);
	
	  
	  // do setup for required DFT images and arrays
	  
      IplImage* realInput = cvCreateImage( cvGetSize(img), IPL_DEPTH_64F, 1);
      IplImage* imaginaryInput = cvCreateImage( cvGetSize(img), IPL_DEPTH_64F, 1);
      IplImage* complexInput = cvCreateImage( cvGetSize(img), IPL_DEPTH_64F, 2);		
		
	  //int dft_M = cvGetOptimalDFTSize( img->height  );
      //int dft_N = cvGetOptimalDFTSize( img->width );

	  int dft_M=768;
	  int dft_N=1024;
	  
	  printf("Optimal size for DFT is height = %i, width = %i\n", dft_M, dft_N);

      CvMat* dft_A = cvCreateMat( dft_M, dft_N, CV_64FC2 );
	  CvMat* dft_Filter = cvCreateMat( dft_M, dft_N, CV_64FC2 );

	  CvMat tmp;
	  double m, M;
      IplImage* image_Re = cvCreateImage( cvSize(dft_N, dft_M), IPL_DEPTH_64F, 1);
      IplImage* image_Im = cvCreateImage( cvSize(dft_N, dft_M), IPL_DEPTH_64F, 1);
	  
	  IplImage* image_Re_Filter = cvCreateImage( cvGetSize(dft_Filter), IPL_DEPTH_64F, 1);
	  IplImage* outputImg = cvCreateImage( cvSize(dft_N, dft_M), img->depth, 1);
	
	  
	  // define grayscale image
	  
	  IplImage* grayImg = 
	  			cvCreateImage(cvSize(img->width,img->height), img->depth, 1);
	  grayImg->origin = img->origin;
	  	
			  
			//  img = cvCloneImage(&(IplImage)in_img);// cvLoadImage( "IMG_0295.JPG", CV_LOAD_IMAGE_UNCHANGED);
			
			  if (img->nChannels > 1){
				  cvCvtColor(img, grayImg, CV_BGR2GRAY);
			  } else {
				  grayImg = img;
			  }
		  
		  // convert grayscale image to real part of DTF input	  
			  
		  cvScale(grayImg, realInput, 1.0, 0.0);
 			  
		  // take the natural log of the input (compute log(1 + Mag)
	 	
		   cvAddS( realInput, cvScalarAll(1.0), realInput, NULL ); // 1 + Mag
	 	   cvLog( realInput, realInput ); // log(1 + Mag) 
		 	  
		  // merge with imaginary part (initialised to all zeros)	
			  
    	  cvZero(imaginaryInput);
    	  cvMerge(realInput, imaginaryInput, NULL, NULL, complexInput);
		  
		  // copy A to dft_A and pad dft_A with zeros
			  
		  cvGetSubRect( dft_A, &tmp, cvRect(0,0, grayImg->width,grayImg->height));
		  cvCopy( complexInput, &tmp, NULL );
		  cvGetSubRect( dft_A, &tmp, cvRect(img->width,0, dft_A->cols - grayImg->width, grayImg->height));
		  if ((dft_A->cols - grayImg->width) > 0)			  
		  {
		  	cvZero( &tmp );
		  }
	
		  // set up filter (first channel is real part / second is imaginary)
		  
	  	  create_butterworth_homomorphic_filter(dft_Filter, radiusD, order, 
		  							(high_h_v_TB * 0.01), (low_h_v_TB * 0.01)); 		  
		  
		  // copy and scale for display
		  cvSplit(dft_Filter, image_Re_Filter, NULL, NULL, NULL);
		  cvMinMaxLoc(image_Re_Filter, &m, &M, NULL, NULL, NULL);  		 
		  cvScale(image_Re_Filter, image_Re_Filter, 1.0/(M-m), 1.0*(-m)/(M-m));			  
		  
		  // get the DFT of the original image (scaled) (and shirt quadrants)
	
		  //cvDFT( dft_A, dft_A, CV_DXT_FORWARD, complexInput->height );
		  cvDFT( dft_A, dft_A, (CV_DXT_FORWARD | CV_DXT_SCALE), complexInput->height );

		  cvShiftDFT( dft_A, dft_A );
		  
		  // apply filter (and shift quadrants back)

		  cvMulSpectrums( dft_A, dft_Filter, dft_A, 0);
		  cvShiftDFT( dft_A, dft_A );

		  // compute spectrum magnitude for display
		  
		  if (dft_spec_mag != NULL){
			  cvReleaseImage(&dft_spec_mag);
		  }
		  //dft_spec_mag = create_spectrum_magnitude_display(dft_A, 1);
		  
		  // invert dft
		  
		  cvDFT( dft_A, dft_A, CV_DXT_INV_SCALE, complexInput->height );
		  //cvDFT( dft_A, dft_A, CV_DXT_INVERSE, complexInput->height );

		  // take the ABS of result (as per MATLAB implementation ??)
          // (appears not to effect result - 29/5/09 
		  // cvAbsDiffS(dft_A, dft_A, cvScalarAll(0));
		  
		  // Split Fourier in real and imaginary parts
		  cvSplit( dft_A, image_Re, image_Im, 0, 0 );		 	
		  
		 // take the exp (inverse of natural log of the input)
		  
	 	 cvExp( image_Re, image_Re );
		 
		 cvMinMaxLoc(image_Re, &m, &M, NULL, NULL, NULL);  		 
		 cvScale(image_Re, image_Re, 1.0/(M-m), 1.0*(-m)/(M-m));

		 // convert to 8-bit 255
		 
		 cvMinMaxLoc(image_Re, &m, &M, NULL, NULL, NULL);  
	     cvScale(image_Re, outputImg, 255.0 / (M-m), 1.0*(-m)/(M-m));
		 outputImg->origin = img->origin;

		 if (doHistEq){
		 	//cvEqualizeHist(outputImg, outputImg);
	 	 }

	  cvReleaseMat( &dft_A);
	  cvReleaseMat( &dft_Filter);

	  if (grayImg) { 
		  cvReleaseData( grayImg );
		  cvReleaseImageHeader(&grayImg );
		  cvReleaseImage( &grayImg );
		 
	  }
	  cvReleaseData( realInput );
		  cvReleaseImageHeader(&realInput );
	  cvReleaseImage( &realInput );
		
	  cvReleaseData( imaginaryInput );
		  cvReleaseImageHeader(&imaginaryInput );
	  cvReleaseImage( &imaginaryInput );

	   cvReleaseData( complexInput );
		  cvReleaseImageHeader(&complexInput );
	  cvReleaseImage( &complexInput );

	   cvReleaseData( image_Re  );
		  cvReleaseImageHeader(&image_Re );
	  cvReleaseImage( &image_Re );

	  cvReleaseData( image_Im   );
		  cvReleaseImageHeader(&image_Im  );
	  cvReleaseImage( &image_Im );

	  cvReleaseData(dft_spec_mag    );
		  cvReleaseImageHeader(&dft_spec_mag  );
	  cvReleaseImage( &dft_spec_mag );

	  cvReleaseData(image_Re_Filter);
		  cvReleaseImageHeader(&image_Re_Filter );
	  cvReleaseImage(&image_Re_Filter);

	Mat mat_out(outputImg);
	mat_out.copyTo(out_img);
	mat_out.release();
//	imshow("WWWW",out_img);

	return out_img;
	
}
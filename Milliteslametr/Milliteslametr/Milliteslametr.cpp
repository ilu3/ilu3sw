//**************************************************************
//�����������:      ������� ���� ����, �. ������ ��������
//������:	        ��� ���-3
//������ �������:   1.0
//������������:     ANSI C++ 98
//������:   		22.04.2014
//�������:   		27.09.2015
//**************************************************************/

/** \author ��������� �.�.*/
/** \file Milliteslametr.cpp*/
/** \brief ���������� ������� ��� ������������� ��������� ���������������.*/

#include "Milliteslametr.h"

#define FIRST_CHILD_CONTOUR 2 //<! \define ������ ������� ��������� �������
#define PARENT_CONTOUR 3 //<! \define ������ ������������� �������

Milliteslametr::Milliteslametr()
{
	BIG=-1;
	CIRCLE = -1;
	WINDOW=-1;
	CIRCLE_BASE=-1;
	WINDOW_BASE=-1;
	SMALL=-1;
	SMALL_BASE=-1;
	circle_area=0;
	circle_perim=0;
	f=new Filter();
}

bool compareContourAreas ( std::vector<cv::Point> contour1, std::vector<cv::Point> contour2 ) 
{
    double i = fabs( contourArea(cv::Mat(contour1)) );
    double j = fabs( contourArea(cv::Mat(contour2)) );
    return ( i < j );
}

cv::vector<Point2f> Milliteslametr::convert2Point2f_vector(cv::vector<Point> contour)
{
	cv::vector<Point2f> converted_contour;
	   
	for(unsigned int i=0; i<contour.size(); i++)
	{
		converted_contour.push_back(Point2f((float)contour[i].x, (float)contour[i].y));
	}

	return converted_contour;
}

cv::vector<Point> Milliteslametr::convert2Point_vector(cv::vector<Point2f> contour)
{
	   cv::vector<Point> converted_contour;
	   
	   for(unsigned int i=0; i<contour.size(); i++)
	   {
		   converted_contour.push_back(Point((int)(contour[i].x+0.5), (int)(contour[i].y+0.5)));
	   }

	   return converted_contour;
}

int Milliteslametr::first_numeric_detector(vector<vector<Point>> all_contours_transform_for_draw,vector<vector<Point>> eight_contours_transform_for_draw)
{
	vector<Point> first_numeric_segment_a;
	vector<Point> first_numeric_segment_b;
	vector<Point> first_numeric_segment_c;
	vector<Point> first_numeric_segment_d;
	vector<Point> first_numeric_segment_e;
	vector<Point> first_numeric_segment_f;
	vector<Point> first_numeric_segment_g;
	vector<Point> first_eigth_points;

	bool first_a_inside=false;
	bool first_b_inside=false;
	bool first_c_inside=false;
	bool first_d_inside=false;
	bool first_e_inside=false;
	bool first_f_inside=false;
	bool first_g_inside=false;
	bool eight=false;

	first_numeric_segment_a.push_back(Point(138,300));//
    first_numeric_segment_a.push_back(Point(137,300));
	first_numeric_segment_a.push_back(Point(136,300));
    first_numeric_segment_a.push_back(Point(135,300));
    first_numeric_segment_a.push_back(Point(134,300));
	first_numeric_segment_a.push_back(Point(133,300));
    first_numeric_segment_a.push_back(Point(132,300));
    first_numeric_segment_a.push_back(Point(131,300));//
    first_numeric_segment_a.push_back(Point(130,300));
    first_numeric_segment_a.push_back(Point(129,300));
	first_numeric_segment_a.push_back(Point(128,300));
    first_numeric_segment_a.push_back(Point(127,300));
    first_numeric_segment_a.push_back(Point(126,300));//

	first_numeric_segment_b.push_back(Point(150,281));//
	first_numeric_segment_b.push_back(Point(150,282));
    first_numeric_segment_b.push_back(Point(150,283));
	first_numeric_segment_b.push_back(Point(150,284));
    first_numeric_segment_b.push_back(Point(150,285));
    first_numeric_segment_b.push_back(Point(150,286));
	first_numeric_segment_b.push_back(Point(150,287));
    first_numeric_segment_b.push_back(Point(150,288));//
    first_numeric_segment_b.push_back(Point(150,289));
    first_numeric_segment_b.push_back(Point(150,290));
	first_numeric_segment_b.push_back(Point(150,291));
    first_numeric_segment_b.push_back(Point(150,292));
    first_numeric_segment_b.push_back(Point(150,293));
	first_numeric_segment_b.push_back(Point(150,294));
    first_numeric_segment_b.push_back(Point(150,295));//

	first_numeric_segment_c.push_back(Point(192,284));//
    first_numeric_segment_c.push_back(Point(192,285));
	first_numeric_segment_c.push_back(Point(192,286));
    first_numeric_segment_c.push_back(Point(192,287));
    first_numeric_segment_c.push_back(Point(192,288));
	first_numeric_segment_c.push_back(Point(192,289));
    first_numeric_segment_c.push_back(Point(192,290));
	first_numeric_segment_c.push_back(Point(192,291));//
    first_numeric_segment_c.push_back(Point(192,292));
    first_numeric_segment_c.push_back(Point(192,293));
	first_numeric_segment_c.push_back(Point(192,294));
    first_numeric_segment_c.push_back(Point(192,295));
    first_numeric_segment_c.push_back(Point(192,296));
	first_numeric_segment_c.push_back(Point(192,297));
    first_numeric_segment_c.push_back(Point(192,298));
    first_numeric_segment_c.push_back(Point(192,299));//

	first_numeric_segment_d.push_back(Point(218,307));//
	first_numeric_segment_d.push_back(Point(217,307));
    first_numeric_segment_d.push_back(Point(216,307));
    first_numeric_segment_d.push_back(Point(215,307));
	first_numeric_segment_d.push_back(Point(214,307));
    first_numeric_segment_d.push_back(Point(213,307));
    first_numeric_segment_d.push_back(Point(212,307));
	first_numeric_segment_d.push_back(Point(211,307));//
    first_numeric_segment_d.push_back(Point(210,307));
    first_numeric_segment_d.push_back(Point(209,307));
	first_numeric_segment_d.push_back(Point(208,307));
    first_numeric_segment_d.push_back(Point(207,307));
    first_numeric_segment_d.push_back(Point(206,307));
	first_numeric_segment_d.push_back(Point(205,307));//

    first_numeric_segment_e.push_back(Point(192,313));//
    first_numeric_segment_e.push_back(Point(192,314));
	first_numeric_segment_e.push_back(Point(192,315));
    first_numeric_segment_e.push_back(Point(192,316));
    first_numeric_segment_e.push_back(Point(192,317));
	first_numeric_segment_e.push_back(Point(192,318));
	first_numeric_segment_e.push_back(Point(192,319));//
    first_numeric_segment_e.push_back(Point(192,321));
    first_numeric_segment_e.push_back(Point(192,322));
	first_numeric_segment_e.push_back(Point(192,323));
    first_numeric_segment_e.push_back(Point(192,324));
    first_numeric_segment_e.push_back(Point(192,325));
	first_numeric_segment_e.push_back(Point(192,326));//

	first_numeric_segment_f.push_back(Point(150,309));//
	first_numeric_segment_f.push_back(Point(150,310));
    first_numeric_segment_f.push_back(Point(150,311));
    first_numeric_segment_f.push_back(Point(150,312));
	first_numeric_segment_f.push_back(Point(150,313));
    first_numeric_segment_f.push_back(Point(150,314));
    first_numeric_segment_f.push_back(Point(150,315));
	first_numeric_segment_f.push_back(Point(150,316));//
    first_numeric_segment_f.push_back(Point(150,317));
    first_numeric_segment_f.push_back(Point(150,318));
	first_numeric_segment_f.push_back(Point(150,319));
    first_numeric_segment_f.push_back(Point(150,320));
    first_numeric_segment_f.push_back(Point(150,321));
	first_numeric_segment_f.push_back(Point(150,322));//

	first_numeric_segment_g.push_back(Point(181,304));//
    first_numeric_segment_g.push_back(Point(180,304));
    first_numeric_segment_g.push_back(Point(179,304));
	first_numeric_segment_g.push_back(Point(178,304));
	first_numeric_segment_g.push_back(Point(177,304));
    first_numeric_segment_g.push_back(Point(176,304));
    first_numeric_segment_g.push_back(Point(175,304));
	first_numeric_segment_g.push_back(Point(174,304));
    first_numeric_segment_g.push_back(Point(173,304));
	first_numeric_segment_g.push_back(Point(172,304));//
    first_numeric_segment_g.push_back(Point(171,304));
    first_numeric_segment_g.push_back(Point(170,304));
	first_numeric_segment_g.push_back(Point(169,304));
    first_numeric_segment_g.push_back(Point(168,304));
    first_numeric_segment_g.push_back(Point(167,304));//

	first_eigth_points.push_back(Point(150,302));
    first_eigth_points.push_back(Point(192,306));

	int index1=-1;
	int index2=-1;
	for(int i=0; i<eight_contours_transform_for_draw.size(); i++)
	{
		if(pointPolygonTest(eight_contours_transform_for_draw[i],first_eigth_points[0],false)>0)	
		{
			index1=i;
		}
	}
	for(int i=0; i<eight_contours_transform_for_draw.size(); i++)
	{
		if(pointPolygonTest(eight_contours_transform_for_draw[i],first_eigth_points[1],false)>0)	
		{
			index2=i;
		}
	}

	if(index1!=index2&&index1!=-1&&index2!=-1) eight=true;
	else eight=false;

	
    for(int j=0; j<first_numeric_segment_a.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],first_numeric_segment_a[j],false)>0)	
					first_a_inside=true;
		  }
	}
	for(int j=0; j<first_numeric_segment_b.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],first_numeric_segment_b[j],false)>0)	
					first_b_inside=true;
		  }
	}
	for(int j=0; j<first_numeric_segment_c.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],first_numeric_segment_c[j],false)>0)	
					first_c_inside=true;
		  }
	}
	for(int j=0; j<first_numeric_segment_d.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],first_numeric_segment_d[j],false)>0)	
					first_d_inside=true;
		  }
	}
	for(int j=0; j<first_numeric_segment_e.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],first_numeric_segment_e[j],false)>0)	
					first_e_inside=true;
		  }
	}
	for(int j=0; j<first_numeric_segment_f.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],first_numeric_segment_f[j],false)>0)	
					first_f_inside=true;
		  }
	}
	for(int j=0; j<first_numeric_segment_g.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],first_numeric_segment_g[j],false)>0)	
					first_g_inside=true;
		  }

	}
    if(first_a_inside&&first_b_inside&&first_c_inside&&first_d_inside&&first_e_inside&&first_f_inside&&eight==false)
		return 0;
	if(first_b_inside&&first_c_inside&&first_a_inside==false&&first_d_inside==false&&first_e_inside==false&&first_f_inside==false)
		return 1;
	if(first_a_inside&&first_b_inside&&first_g_inside&&first_e_inside&&first_d_inside&&first_c_inside==false&&first_f_inside==false)
		return 2;
	if(first_a_inside&&first_b_inside&&first_c_inside&&first_d_inside&&first_g_inside&&first_f_inside==false&&first_e_inside==false)
		return 3;
	if(first_f_inside&&first_g_inside&&first_b_inside&&first_c_inside&&first_a_inside==false&&first_e_inside==false&&first_d_inside==false)
		return 4;
	if(first_a_inside&&first_f_inside&&first_g_inside&&first_c_inside&&first_d_inside&&first_b_inside==false&&first_e_inside==false)
		return 5;
	if((first_a_inside&&first_f_inside&&first_e_inside&&first_d_inside&&first_c_inside&&first_g_inside&&first_b_inside==false)||
	   (first_a_inside==false&&first_f_inside&&first_e_inside&&first_d_inside&&first_c_inside&&first_g_inside&&first_b_inside==false))
		return 6;
	if(first_a_inside&&first_b_inside&&first_c_inside&&first_d_inside==false&&first_e_inside==false&&first_f_inside==false)
		return 1;//����� ���������� �������� ��������, ���������� ������� ������ �������
	if(first_a_inside&&first_b_inside&&first_c_inside&&first_d_inside&&first_e_inside&&first_f_inside&&first_g_inside&&eight)
		return 8;
	if(first_a_inside&&first_b_inside&&first_c_inside&&first_d_inside&&first_f_inside&&first_g_inside&&first_e_inside==false)
		return 9;
	else 
		return -1;
}
int Milliteslametr::second_numeric_detector(vector<vector<Point>> all_contours_transform_for_draw,vector<vector<Point>> eight_contours_transform_for_draw)
{
	vector<Point> second_numeric_segment_a;
	vector<Point> second_numeric_segment_b;
	vector<Point> second_numeric_segment_c;
	vector<Point> second_numeric_segment_d;
	vector<Point> second_numeric_segment_e;
	vector<Point> second_numeric_segment_f;
	vector<Point> second_numeric_segment_g;
	vector<Point> second_eigth_points;

	bool second_a_inside=false;
	bool second_b_inside=false;
	bool second_c_inside=false;
	bool second_d_inside=false;
	bool second_e_inside=false;
	bool second_f_inside=false;
	bool second_g_inside=false;
	bool eight=false;

	second_numeric_segment_a.push_back(Point(139,249));//
	second_numeric_segment_a.push_back(Point(138,249));
    second_numeric_segment_a.push_back(Point(137,249));
	second_numeric_segment_a.push_back(Point(136,249));
    second_numeric_segment_a.push_back(Point(135,249));
    second_numeric_segment_a.push_back(Point(134,249));
	second_numeric_segment_a.push_back(Point(133,249));
    second_numeric_segment_a.push_back(Point(132,249));//
    second_numeric_segment_a.push_back(Point(131,249));
    second_numeric_segment_a.push_back(Point(130,249));
	second_numeric_segment_a.push_back(Point(129,249));
    second_numeric_segment_a.push_back(Point(128,249));
    second_numeric_segment_a.push_back(Point(127,249));
	second_numeric_segment_a.push_back(Point(126,249));
    second_numeric_segment_a.push_back(Point(125,249));//

    second_numeric_segment_b.push_back(Point(151,229));//
	second_numeric_segment_b.push_back(Point(151,230));
    second_numeric_segment_b.push_back(Point(151,231));
    second_numeric_segment_b.push_back(Point(151,232));
	second_numeric_segment_b.push_back(Point(151,233));
    second_numeric_segment_b.push_back(Point(151,234));
    second_numeric_segment_b.push_back(Point(151,235));
    second_numeric_segment_b.push_back(Point(151,236));//
    second_numeric_segment_b.push_back(Point(151,237));
    second_numeric_segment_b.push_back(Point(151,238));
	second_numeric_segment_b.push_back(Point(151,239));
    second_numeric_segment_b.push_back(Point(151,240));
    second_numeric_segment_b.push_back(Point(151,241));
	second_numeric_segment_b.push_back(Point(151,242));//

	second_numeric_segment_c.push_back(Point(192,229));//
    second_numeric_segment_c.push_back(Point(192,230));
    second_numeric_segment_c.push_back(Point(192,231));
	second_numeric_segment_c.push_back(Point(192,232));
    second_numeric_segment_c.push_back(Point(192,233));
    second_numeric_segment_c.push_back(Point(192,234));
	second_numeric_segment_c.push_back(Point(192,235));
    second_numeric_segment_c.push_back(Point(192,236));
	second_numeric_segment_c.push_back(Point(192,237));//
    second_numeric_segment_c.push_back(Point(192,238));
    second_numeric_segment_c.push_back(Point(192,239));
	second_numeric_segment_c.push_back(Point(192,240));
    second_numeric_segment_c.push_back(Point(192,241));
    second_numeric_segment_c.push_back(Point(192,242));
	second_numeric_segment_c.push_back(Point(192,243));
    second_numeric_segment_c.push_back(Point(192,244));
    second_numeric_segment_c.push_back(Point(192,245));//

	second_numeric_segment_d.push_back(Point(219,255));//
    second_numeric_segment_d.push_back(Point(218,255));
	second_numeric_segment_d.push_back(Point(217,255));
    second_numeric_segment_d.push_back(Point(216,255));
    second_numeric_segment_d.push_back(Point(215,255));
	second_numeric_segment_d.push_back(Point(214,255));
    second_numeric_segment_d.push_back(Point(213,255));
    second_numeric_segment_d.push_back(Point(212,255));
	second_numeric_segment_d.push_back(Point(211,255));//
    second_numeric_segment_d.push_back(Point(210,255));
    second_numeric_segment_d.push_back(Point(209,255));
	second_numeric_segment_d.push_back(Point(208,255));
    second_numeric_segment_d.push_back(Point(207,255));
    second_numeric_segment_d.push_back(Point(206,255));
	second_numeric_segment_d.push_back(Point(205,255));//

	second_numeric_segment_e.push_back(Point(192,274));//
	second_numeric_segment_e.push_back(Point(192,273));
	second_numeric_segment_e.push_back(Point(192,272));
    second_numeric_segment_e.push_back(Point(192,271));
	second_numeric_segment_e.push_back(Point(192,270));
    second_numeric_segment_e.push_back(Point(192,269));
    second_numeric_segment_e.push_back(Point(192,268));
	second_numeric_segment_e.push_back(Point(192,267));//
    second_numeric_segment_e.push_back(Point(192,266));
    second_numeric_segment_e.push_back(Point(192,265));
	second_numeric_segment_e.push_back(Point(192,264));
    second_numeric_segment_e.push_back(Point(192,263));
    second_numeric_segment_e.push_back(Point(192,262));
	second_numeric_segment_e.push_back(Point(192,261));//

	second_numeric_segment_f.push_back(Point(151,259));//
    second_numeric_segment_f.push_back(Point(151,260));
	second_numeric_segment_f.push_back(Point(151,261));
	second_numeric_segment_f.push_back(Point(151,262));
    second_numeric_segment_f.push_back(Point(151,263));
	second_numeric_segment_f.push_back(Point(151,264));
    second_numeric_segment_f.push_back(Point(151,265));
	second_numeric_segment_f.push_back(Point(151,266));
    second_numeric_segment_f.push_back(Point(151,267));
	second_numeric_segment_f.push_back(Point(151,268));//
    second_numeric_segment_f.push_back(Point(151,269));
    second_numeric_segment_f.push_back(Point(151,270));
	second_numeric_segment_f.push_back(Point(151,271));
    second_numeric_segment_f.push_back(Point(151,272));
    second_numeric_segment_f.push_back(Point(151,273));//

	second_numeric_segment_g.push_back(Point(178,251));//
    second_numeric_segment_g.push_back(Point(177,251));
    second_numeric_segment_g.push_back(Point(176,251));
	second_numeric_segment_g.push_back(Point(175,251));
    second_numeric_segment_g.push_back(Point(174,251));
    second_numeric_segment_g.push_back(Point(173,251));
	second_numeric_segment_g.push_back(Point(172,251));//
    second_numeric_segment_g.push_back(Point(171,251));
    second_numeric_segment_g.push_back(Point(170,251));
	second_numeric_segment_g.push_back(Point(169,251));
    second_numeric_segment_g.push_back(Point(168,251));
    second_numeric_segment_g.push_back(Point(167,251));
	second_numeric_segment_g.push_back(Point(166,251));//

	second_eigth_points.push_back(Point(151,251));
    second_eigth_points.push_back(Point(193,254));

	int index1=-1;
	int index2=-1;
	for(int i=0; i<eight_contours_transform_for_draw.size(); i++)
	{
		if(pointPolygonTest(eight_contours_transform_for_draw[i],second_eigth_points[0],false)>0)	
		{
			index1=i;
		}
	}
	for(int i=0; i<eight_contours_transform_for_draw.size(); i++)
	{
		if(pointPolygonTest(eight_contours_transform_for_draw[i],second_eigth_points[1],false)>0)	
		{
			index2=i;
		}
	}

	if(index1!=index2&&index1!=-1&&index2!=-1) eight=true;
	else eight=false;

    for(int j=0; j<second_numeric_segment_a.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],second_numeric_segment_a[j],false)>0)	
					second_a_inside=true;
		  }
	}
	for(int j=0; j<second_numeric_segment_b.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],second_numeric_segment_b[j],false)>0)	
					second_b_inside=true;
		  }
	}
	for(int j=0; j<second_numeric_segment_c.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],second_numeric_segment_c[j],false)>0)	
					second_c_inside=true;
		  }
	}
	for(int j=0; j<second_numeric_segment_d.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],second_numeric_segment_d[j],false)>0)	
					second_d_inside=true;
		  }
	}
	for(int j=0; j<second_numeric_segment_e.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],second_numeric_segment_e[j],false)>0)	
					second_e_inside=true;
		  }
	}
	for(int j=0; j<second_numeric_segment_f.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],second_numeric_segment_f[j],false)>0)	
					second_f_inside=true;
		  }
	}
	for(int j=0; j<second_numeric_segment_g.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],second_numeric_segment_g[j],false)>0)	
					second_g_inside=true;
		  }

	}

    if(second_a_inside&&second_b_inside&&second_c_inside&&second_d_inside&&second_e_inside&&second_f_inside&&eight==false)
		return 0;
	if(second_b_inside&&second_c_inside&&second_a_inside==false&&second_d_inside==false&&second_e_inside==false&&second_f_inside==false&&second_g_inside==false)
		return 1;
	if(second_a_inside&&second_b_inside&&second_g_inside&&second_e_inside&&second_d_inside&&second_c_inside==false&&second_f_inside==false)
		return 2;
	if(second_a_inside&&second_b_inside&&second_c_inside&&second_d_inside&&second_g_inside&&second_f_inside==false&&second_e_inside==false)
		return 3;
	if(second_f_inside&&second_g_inside&&second_b_inside&&second_c_inside&&second_a_inside==false&&second_e_inside==false&&second_d_inside==false)
		return 4;
	if(second_a_inside&&second_f_inside&&second_g_inside&&second_c_inside&&second_d_inside&&second_b_inside==false&&second_e_inside==false)
		return 5;
	if((second_a_inside&&second_f_inside&&second_e_inside&&second_d_inside&&second_c_inside&&second_g_inside&&second_b_inside==false)||
		(second_a_inside==false&&second_f_inside&&second_e_inside&&second_d_inside&&second_c_inside&&second_g_inside&&second_b_inside==false))
		return 6;
	if(second_a_inside&&second_b_inside&&second_c_inside&&second_d_inside==false&&second_e_inside==false&&second_f_inside==false&&second_g_inside==false)
		return 7;
	if(second_a_inside&&second_b_inside&&second_c_inside&&second_d_inside&&second_e_inside&&second_f_inside&&second_g_inside&&eight)
		return 8;
	if(second_a_inside&&second_b_inside&&second_c_inside&&second_d_inside&&second_f_inside&&second_g_inside&&second_e_inside==false)
		return 9;
	else 
		return -1;
}

int Milliteslametr::third_numeric_detector(vector<vector<Point>> all_contours_transform_for_draw,vector<vector<Point>> eight_contours_transform_for_draw)
{
	vector<Point> third_numeric_segment_a;
	vector<Point> third_numeric_segment_b;
	vector<Point> third_numeric_segment_c;
	vector<Point> third_numeric_segment_d;
	vector<Point> third_numeric_segment_e;
	vector<Point> third_numeric_segment_f;
	vector<Point> third_numeric_segment_g;
	vector<Point> third_eigth_points;

	bool third_a_inside=false;
	bool third_b_inside=false;
	bool third_c_inside=false;
	bool third_d_inside=false;
	bool third_e_inside=false;
	bool third_f_inside=false;
	bool third_g_inside=false;
	bool eight=false;

	third_numeric_segment_a.push_back(Point(138,198));//
    third_numeric_segment_a.push_back(Point(137,198));
	third_numeric_segment_a.push_back(Point(136,198));
    third_numeric_segment_a.push_back(Point(135,198));
	third_numeric_segment_a.push_back(Point(134,198));
    third_numeric_segment_a.push_back(Point(133,198));
    third_numeric_segment_a.push_back(Point(132,198));//
    third_numeric_segment_a.push_back(Point(131,198));
    third_numeric_segment_a.push_back(Point(130,198));
	third_numeric_segment_a.push_back(Point(129,198));
    third_numeric_segment_a.push_back(Point(128,198));
	third_numeric_segment_a.push_back(Point(127,198));
    third_numeric_segment_a.push_back(Point(126,198));
	third_numeric_segment_a.push_back(Point(125,198));
    third_numeric_segment_a.push_back(Point(124,198));//

	third_numeric_segment_b.push_back(Point(151,178));//
    third_numeric_segment_b.push_back(Point(151,179));
	third_numeric_segment_b.push_back(Point(151,180));
    third_numeric_segment_b.push_back(Point(151,181));
    third_numeric_segment_b.push_back(Point(151,182));
	third_numeric_segment_b.push_back(Point(151,183));
    third_numeric_segment_b.push_back(Point(151,184));
    third_numeric_segment_b.push_back(Point(151,185));//
    third_numeric_segment_b.push_back(Point(151,186));
    third_numeric_segment_b.push_back(Point(151,187));
	third_numeric_segment_b.push_back(Point(151,188));
    third_numeric_segment_b.push_back(Point(151,189));
    third_numeric_segment_b.push_back(Point(151,190));
	third_numeric_segment_b.push_back(Point(151,191));
    third_numeric_segment_b.push_back(Point(151,192));
    third_numeric_segment_b.push_back(Point(151,193));//

	third_numeric_segment_c.push_back(Point(193,182));//
	third_numeric_segment_c.push_back(Point(193,183));
	third_numeric_segment_c.push_back(Point(193,184));
	third_numeric_segment_c.push_back(Point(193,185));
	third_numeric_segment_c.push_back(Point(193,186));
	third_numeric_segment_c.push_back(Point(193,187));
	third_numeric_segment_c.push_back(Point(193,188));
	third_numeric_segment_c.push_back(Point(193,189));//
    third_numeric_segment_c.push_back(Point(193,190));
    third_numeric_segment_c.push_back(Point(193,191));
	third_numeric_segment_c.push_back(Point(193,192));
    third_numeric_segment_c.push_back(Point(193,193));
	third_numeric_segment_c.push_back(Point(193,194));
    third_numeric_segment_c.push_back(Point(193,195));
	third_numeric_segment_c.push_back(Point(193,196));//

	third_numeric_segment_d.push_back(Point(220,203));//
    third_numeric_segment_d.push_back(Point(219,203));
	third_numeric_segment_d.push_back(Point(218,203));
	third_numeric_segment_d.push_back(Point(217,203));
    third_numeric_segment_d.push_back(Point(216,203));
	third_numeric_segment_d.push_back(Point(215,203));
    third_numeric_segment_d.push_back(Point(214,203));
	third_numeric_segment_d.push_back(Point(213,203));
	third_numeric_segment_d.push_back(Point(212,203));//
    third_numeric_segment_d.push_back(Point(211,203));
    third_numeric_segment_d.push_back(Point(210,203));
	third_numeric_segment_d.push_back(Point(209,203));
    third_numeric_segment_d.push_back(Point(208,203));
	third_numeric_segment_d.push_back(Point(207,203));
    third_numeric_segment_d.push_back(Point(206,203));//

	third_numeric_segment_e.push_back(Point(192,222));//
	third_numeric_segment_e.push_back(Point(192,221));
    third_numeric_segment_e.push_back(Point(192,220));
    third_numeric_segment_e.push_back(Point(192,219));
	third_numeric_segment_e.push_back(Point(192,218));
    third_numeric_segment_e.push_back(Point(192,217));
	third_numeric_segment_e.push_back(Point(192,216));//
    third_numeric_segment_e.push_back(Point(192,215));
    third_numeric_segment_e.push_back(Point(192,214));
	third_numeric_segment_e.push_back(Point(192,213));
    third_numeric_segment_e.push_back(Point(192,212));
    third_numeric_segment_e.push_back(Point(192,211));
	third_numeric_segment_e.push_back(Point(192,210));
    third_numeric_segment_e.push_back(Point(192,209));//

	third_numeric_segment_f.push_back(Point(152,220));//
	third_numeric_segment_f.push_back(Point(152,219));
	third_numeric_segment_f.push_back(Point(152,218));
	third_numeric_segment_f.push_back(Point(152,217));
	third_numeric_segment_f.push_back(Point(152,216));
	third_numeric_segment_f.push_back(Point(152,215));
	third_numeric_segment_f.push_back(Point(152,214));
	third_numeric_segment_f.push_back(Point(152,213));//
    third_numeric_segment_f.push_back(Point(152,212));
    third_numeric_segment_f.push_back(Point(152,211));
	third_numeric_segment_f.push_back(Point(152,210));
    third_numeric_segment_f.push_back(Point(152,209));
    third_numeric_segment_f.push_back(Point(152,208));
	third_numeric_segment_f.push_back(Point(152,206));//

	third_numeric_segment_g.push_back(Point(178,200));//
	third_numeric_segment_g.push_back(Point(177,200));
	third_numeric_segment_g.push_back(Point(176,200));
    third_numeric_segment_g.push_back(Point(175,200));
    third_numeric_segment_g.push_back(Point(174,200));
	third_numeric_segment_g.push_back(Point(173,200));
    third_numeric_segment_g.push_back(Point(172,200));
	third_numeric_segment_g.push_back(Point(171,200));//
    third_numeric_segment_g.push_back(Point(170,200));
    third_numeric_segment_g.push_back(Point(169,200));
	third_numeric_segment_g.push_back(Point(168,200));
    third_numeric_segment_g.push_back(Point(167,200));
    third_numeric_segment_g.push_back(Point(166,200));//

	third_eigth_points.push_back(Point(150,200));
    third_eigth_points.push_back(Point(191,203));

	int index1=-1;
	int index2=-1;
	for(int i=0; i<eight_contours_transform_for_draw.size(); i++)
	{
		if(pointPolygonTest(eight_contours_transform_for_draw[i],third_eigth_points[0],false)>0)	
		{
			index1=i;
		}
	}
	for(int i=0; i<eight_contours_transform_for_draw.size(); i++)
	{
		if(pointPolygonTest(eight_contours_transform_for_draw[i],third_eigth_points[1],false)>0)	
		{
			index2=i;
		}
	}

	if(index1!=index2&&index1!=-1&&index2!=-1) eight=true;
	else eight=false;

	for(int j=0; j<third_numeric_segment_a.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],third_numeric_segment_a[j],false)>0)	
					third_a_inside=true;
		  }
	}
	for(int j=0; j<third_numeric_segment_b.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],third_numeric_segment_b[j],false)>0)	
					third_b_inside=true;
		  }
	}
	for(int j=0; j<third_numeric_segment_c.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],third_numeric_segment_c[j],false)>0)	
					third_c_inside=true;
		  }
	}
	for(int j=0; j<third_numeric_segment_d.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],third_numeric_segment_d[j],false)>0)	
					third_d_inside=true;
		  }
	}
	for(int j=0; j<third_numeric_segment_e.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],third_numeric_segment_e[j],false)>0)	
					third_e_inside=true;
		  }
	}
	for(int j=0; j<third_numeric_segment_f.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],third_numeric_segment_f[j],false)>0)	
					third_f_inside=true;
		  }
	}
	for(int j=0; j<third_numeric_segment_g.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],third_numeric_segment_g[j],false)>0)	
					third_g_inside=true;
		  }
	}

    if(third_a_inside&&third_b_inside&&third_c_inside&&third_d_inside&&third_e_inside&&third_f_inside&&eight==false)
		return 0;
	if(third_b_inside&&third_c_inside&&third_a_inside==false&&third_d_inside==false&&third_e_inside==false&&third_f_inside==false&&third_g_inside==false)
		return 1;
	if(third_a_inside&&third_b_inside&&third_g_inside&&third_e_inside&&third_d_inside&&third_c_inside==false&&third_f_inside==false)
		return 2;
	if(third_a_inside&&third_b_inside&&third_c_inside&&third_d_inside&&third_g_inside&&third_f_inside==false&&third_e_inside==false)
		return 3;
	if(third_f_inside&&third_g_inside&&third_b_inside&&third_c_inside&&third_a_inside==false&&third_e_inside==false&&third_d_inside==false)
		return 4;
	if(third_a_inside&&third_f_inside&&third_g_inside&&third_c_inside&&third_d_inside&&third_b_inside==false&&third_e_inside==false)
		return 5;
	if((third_a_inside&&third_f_inside&&third_e_inside&&third_d_inside&&third_c_inside&&third_g_inside&&third_b_inside==false)||
		(third_a_inside==false&&third_f_inside&&third_e_inside&&third_d_inside&&third_c_inside&&third_g_inside&&third_b_inside==false))
		return 6;
	if(third_a_inside&&third_b_inside&&third_c_inside&&third_d_inside==false&&third_e_inside==false&&third_f_inside==false&&third_g_inside==false)
		return 7;
	if(third_a_inside&&third_b_inside&&third_c_inside&&third_d_inside&&third_e_inside&&third_f_inside&&third_g_inside&&eight)
		return 8;
	if(third_a_inside&&third_b_inside&&third_c_inside&&third_d_inside&&third_f_inside&&third_g_inside&&third_e_inside==false)
		return 9;
	else 
		return -1;
}

int Milliteslametr::fourth_numeric_detector(vector<vector<Point>> all_contours_transform_for_draw,vector<vector<Point>> eight_contours_transform_for_draw)
{

	vector<Point> fourth_numeric_segment_a;
	vector<Point> fourth_numeric_segment_b;
	vector<Point> fourth_numeric_segment_c;
	vector<Point> fourth_numeric_segment_d;
	vector<Point> fourth_numeric_segment_e;
	vector<Point> fourth_numeric_segment_f;
	vector<Point> fourth_numeric_segment_g;
	vector<Point> fourth_eigth_points;

	bool fourth_a_inside=false;
	bool fourth_b_inside=false;
	bool fourth_c_inside=false;
	bool fourth_d_inside=false;
	bool fourth_e_inside=false;
	bool fourth_f_inside=false;
	bool fourth_g_inside=false;
	bool eight=false;
	bool eight1=false;
	bool eight2=false;

	fourth_numeric_segment_a.push_back(Point(124,145));
	fourth_numeric_segment_a.push_back(Point(126,145));
    fourth_numeric_segment_a.push_back(Point(127,145));
	fourth_numeric_segment_a.push_back(Point(128,145));
    fourth_numeric_segment_a.push_back(Point(129,145));
    fourth_numeric_segment_a.push_back(Point(131,145));
    fourth_numeric_segment_a.push_back(Point(132,145));//
    fourth_numeric_segment_a.push_back(Point(133,145));
    fourth_numeric_segment_a.push_back(Point(134,145));
	fourth_numeric_segment_a.push_back(Point(135,145));
    fourth_numeric_segment_a.push_back(Point(136,145));
    fourth_numeric_segment_a.push_back(Point(137,145));

	fourth_numeric_segment_b.push_back(Point(150,125));
    fourth_numeric_segment_b.push_back(Point(150,126));
	fourth_numeric_segment_b.push_back(Point(150,127));
    fourth_numeric_segment_b.push_back(Point(150,128));
    fourth_numeric_segment_b.push_back(Point(150,129));
	fourth_numeric_segment_b.push_back(Point(150,130));
    fourth_numeric_segment_b.push_back(Point(150,131));
    fourth_numeric_segment_b.push_back(Point(150,132));
	fourth_numeric_segment_b.push_back(Point(150,133));
    fourth_numeric_segment_b.push_back(Point(150,134));//
    fourth_numeric_segment_b.push_back(Point(150,135));
    fourth_numeric_segment_b.push_back(Point(150,136));
	fourth_numeric_segment_b.push_back(Point(150,137));
    fourth_numeric_segment_b.push_back(Point(150,138));
    fourth_numeric_segment_b.push_back(Point(150,139));
	fourth_numeric_segment_b.push_back(Point(150,140));

	fourth_numeric_segment_c.push_back(Point(192,129));
	fourth_numeric_segment_c.push_back(Point(192,130));
	fourth_numeric_segment_c.push_back(Point(192,131));
	fourth_numeric_segment_c.push_back(Point(192,132));
	fourth_numeric_segment_c.push_back(Point(192,133));
	fourth_numeric_segment_c.push_back(Point(192,134));
	fourth_numeric_segment_c.push_back(Point(192,135));
	fourth_numeric_segment_c.push_back(Point(192,136));
	fourth_numeric_segment_c.push_back(Point(192,137));//
    fourth_numeric_segment_c.push_back(Point(192,138));
    fourth_numeric_segment_c.push_back(Point(192,139));
	fourth_numeric_segment_c.push_back(Point(192,140));
    fourth_numeric_segment_c.push_back(Point(192,141));
    fourth_numeric_segment_c.push_back(Point(192,142));
	fourth_numeric_segment_c.push_back(Point(192,143));
    fourth_numeric_segment_c.push_back(Point(192,144));
	fourth_numeric_segment_c.push_back(Point(192,145));
    fourth_numeric_segment_c.push_back(Point(192,146));
    

	fourth_numeric_segment_d.push_back(Point(218,154));//
    fourth_numeric_segment_d.push_back(Point(217,154));
    fourth_numeric_segment_d.push_back(Point(216,154));
	fourth_numeric_segment_d.push_back(Point(215,154));
    fourth_numeric_segment_d.push_back(Point(214,154));
    fourth_numeric_segment_d.push_back(Point(213,154));
	fourth_numeric_segment_d.push_back(Point(212,154));
	fourth_numeric_segment_d.push_back(Point(211,154));//
    fourth_numeric_segment_d.push_back(Point(210,154));
    fourth_numeric_segment_d.push_back(Point(209,154));
	fourth_numeric_segment_d.push_back(Point(208,154));
    fourth_numeric_segment_d.push_back(Point(207,154));
    fourth_numeric_segment_d.push_back(Point(206,154));
	fourth_numeric_segment_d.push_back(Point(205,154));//
	fourth_numeric_segment_d.push_back(Point(218,152));
    fourth_numeric_segment_d.push_back(Point(217,152));
    fourth_numeric_segment_d.push_back(Point(216,152));
	fourth_numeric_segment_d.push_back(Point(215,152));
    fourth_numeric_segment_d.push_back(Point(214,152));
    fourth_numeric_segment_d.push_back(Point(213,152));
	fourth_numeric_segment_d.push_back(Point(212,152));
	fourth_numeric_segment_d.push_back(Point(211,152));//
    fourth_numeric_segment_d.push_back(Point(210,152));
    fourth_numeric_segment_d.push_back(Point(209,152));
	fourth_numeric_segment_d.push_back(Point(208,152));
    fourth_numeric_segment_d.push_back(Point(207,152));
    fourth_numeric_segment_d.push_back(Point(206,152));
	fourth_numeric_segment_d.push_back(Point(205,152));//


    fourth_numeric_segment_e.push_back(Point(192,171));
    fourth_numeric_segment_e.push_back(Point(192,170));
	fourth_numeric_segment_e.push_back(Point(192,169));
    fourth_numeric_segment_e.push_back(Point(192,168));
    fourth_numeric_segment_e.push_back(Point(192,167));
	fourth_numeric_segment_e.push_back(Point(192,166));//
    fourth_numeric_segment_e.push_back(Point(192,165));
    fourth_numeric_segment_e.push_back(Point(192,164));
	fourth_numeric_segment_e.push_back(Point(192,163));
    fourth_numeric_segment_e.push_back(Point(192,162));
    fourth_numeric_segment_e.push_back(Point(192,161));
	fourth_numeric_segment_e.push_back(Point(192,160));
    fourth_numeric_segment_e.push_back(Point(192,159));
    fourth_numeric_segment_e.push_back(Point(192,158));//

	fourth_numeric_segment_f.push_back(Point(151,156));//
	fourth_numeric_segment_f.push_back(Point(151,157));
    fourth_numeric_segment_f.push_back(Point(151,158));
	fourth_numeric_segment_f.push_back(Point(151,159));
    fourth_numeric_segment_f.push_back(Point(151,160));
	fourth_numeric_segment_f.push_back(Point(151,161));
	fourth_numeric_segment_f.push_back(Point(151,162));//
    fourth_numeric_segment_f.push_back(Point(151,163));
    fourth_numeric_segment_f.push_back(Point(151,164));
	fourth_numeric_segment_f.push_back(Point(151,165));
    fourth_numeric_segment_f.push_back(Point(151,166));
	fourth_numeric_segment_f.push_back(Point(151,167));//

	fourth_numeric_segment_g.push_back(Point(178,150));//
	fourth_numeric_segment_g.push_back(Point(177,150));
    fourth_numeric_segment_g.push_back(Point(176,150));
	fourth_numeric_segment_g.push_back(Point(175,150));
    fourth_numeric_segment_g.push_back(Point(174,150));
    fourth_numeric_segment_g.push_back(Point(173,150));
	fourth_numeric_segment_g.push_back(Point(172,150));//
    fourth_numeric_segment_g.push_back(Point(171,150));
    fourth_numeric_segment_g.push_back(Point(170,150));
	fourth_numeric_segment_g.push_back(Point(169,150));
    fourth_numeric_segment_g.push_back(Point(168,150));
    fourth_numeric_segment_g.push_back(Point(167,150));
	fourth_numeric_segment_g.push_back(Point(166,150));//
	fourth_numeric_segment_g.push_back(Point(178,148));//
	fourth_numeric_segment_g.push_back(Point(177,148));
    fourth_numeric_segment_g.push_back(Point(176,148));
	fourth_numeric_segment_g.push_back(Point(175,148));
    fourth_numeric_segment_g.push_back(Point(174,148));
    fourth_numeric_segment_g.push_back(Point(173,148));
	fourth_numeric_segment_g.push_back(Point(172,148));//
    fourth_numeric_segment_g.push_back(Point(171,148));
    fourth_numeric_segment_g.push_back(Point(170,148));
	fourth_numeric_segment_g.push_back(Point(169,148));
    fourth_numeric_segment_g.push_back(Point(168,148));
    fourth_numeric_segment_g.push_back(Point(167,148));
	fourth_numeric_segment_g.push_back(Point(166,148));//

	fourth_eigth_points.push_back(Point(151,148));
    fourth_eigth_points.push_back(Point(192,152));

	int index1=-1;
	int index2=-1;
	for(int i=0; i<eight_contours_transform_for_draw.size(); i++)
	{
		if(pointPolygonTest(eight_contours_transform_for_draw[i],fourth_eigth_points[0],false)>0)	
		{
			eight1=true;
			index1=i;
		}
	}
	for(int i=0; i<eight_contours_transform_for_draw.size(); i++)
	{
		if(pointPolygonTest(eight_contours_transform_for_draw[i],fourth_eigth_points[1],false)>0)	
		{
			eight2=true;
			index2=i;
		}
	}

	if(index1!=index2&&index1!=-1&&index2!=-1) eight=true;
	else eight=false;

    for(int j=0; j<fourth_numeric_segment_a.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],fourth_numeric_segment_a[j],false)>0)	
				{ fourth_a_inside=true;  }
		  }
	}
	for(int j=0; j<fourth_numeric_segment_b.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],fourth_numeric_segment_b[j],false)>0)	
					fourth_b_inside=true;
		  }
	}
	for(int j=0; j<fourth_numeric_segment_c.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],fourth_numeric_segment_c[j],false)>0)	
					fourth_c_inside=true;
		  }
	}
	for(int j=0; j<fourth_numeric_segment_d.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],fourth_numeric_segment_d[j],false)>0)	
					fourth_d_inside=true;
		  }
	}			
	for(int j=0; j<fourth_numeric_segment_e.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],fourth_numeric_segment_e[j],false)>0)	
					fourth_e_inside=true;
		  }
	}			
	for(int j=0; j<fourth_numeric_segment_f.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],fourth_numeric_segment_f[j],false)>0)	
					fourth_f_inside=true;
		  }
	}			
	for(int j=0; j<fourth_numeric_segment_g.size(); j++)
    {
		  for(int i=0; i<all_contours_transform_for_draw.size(); i++)
		  {
				if(pointPolygonTest(all_contours_transform_for_draw[i],fourth_numeric_segment_g[j],false)>0)	
					fourth_g_inside=true;
		  }
	}				
				

    if(fourth_a_inside&&fourth_b_inside&&fourth_c_inside&&fourth_d_inside&&fourth_e_inside&&fourth_f_inside&&eight==false)
		return 0;
	if(fourth_b_inside&&fourth_c_inside&&fourth_a_inside==false&&fourth_d_inside==false&&fourth_e_inside==false&&fourth_f_inside==false&&fourth_g_inside==false)
		return 1;
	if(fourth_a_inside&&fourth_b_inside&&fourth_g_inside&&fourth_e_inside&&fourth_d_inside&&fourth_c_inside==false&&fourth_f_inside==false)
		return 2;
	if(fourth_a_inside&&fourth_b_inside&&fourth_c_inside&&fourth_d_inside&&fourth_g_inside&&fourth_f_inside==false&&fourth_e_inside==false)
		return 3;
	if(fourth_f_inside&&fourth_g_inside&&fourth_b_inside&&fourth_c_inside&&fourth_a_inside==false&&fourth_e_inside==false&&fourth_d_inside==false)
		return 4;
	if(fourth_a_inside&&fourth_f_inside&&fourth_g_inside&&fourth_c_inside&&fourth_d_inside&&fourth_b_inside==false&&fourth_e_inside==false)
		return 5;
	if((fourth_a_inside&&fourth_f_inside&&fourth_e_inside&&fourth_d_inside&&fourth_c_inside&&fourth_g_inside&&fourth_b_inside==false)||
		(fourth_a_inside==false&&fourth_f_inside&&fourth_e_inside&&fourth_d_inside&&fourth_c_inside&&fourth_g_inside&&fourth_b_inside==false))
		return 6;
	if(fourth_a_inside&&fourth_b_inside&&fourth_c_inside&&fourth_d_inside==false&&fourth_e_inside==false&&fourth_f_inside==false&&fourth_g_inside==false)
		return 7;
	if(fourth_a_inside&&fourth_b_inside&&fourth_c_inside&&fourth_d_inside&&fourth_e_inside&&fourth_f_inside&&fourth_g_inside&&eight)
		return 8;
	if(fourth_a_inside&&fourth_b_inside&&fourth_c_inside&&fourth_d_inside&&fourth_f_inside&&fourth_g_inside&&fourth_e_inside==false)
		return 9;
	else 
		return -1;
}

float Milliteslametr::leng(Point2f point1,Point2f point2)
{
	vector<Point> line;
	line.push_back(point1);
	line.push_back(point2);
	float leng=arcLength(line, true );
	line.clear();
	return leng;
}

Mat image_buf;///<�������� �����������, ��������� ����������� � ������ � ����������� 1024*768

VideoCapture capture;///<������ ������ ������� ����������� � ������ � ����������� 1024*768
HANDLE hThread;///<���������� ������
DWORD dwThreadID;///<������������� ������

VideoCapture capture2;///<������ ������ ������� ����������� � ������ � ����������� 640*480
HANDLE hThread2;///<���������� ������
DWORD dwThreadID2;///<������������� ������

CRITICAL_SECTION cr_sec2;///<����������� ������, ������������ ������ � ���������� image_buf �� ����� ��������� �����������
CRITICAL_SECTION cr_sec;///<����������� ������, ������������ ������ � ���������� image_buf �� ����� ������� ����������� � ������
CRITICAL_SECTION cr_sec3;///<����������� ������, ������������ ������ � ���������� image_buf �� ����� ������� ����������� � ������

DWORD WINAPI Camera1024(PVOID param)
{

	InitializeCriticalSection(&cr_sec);
	while(true)
	{
		int count = 0;
		int count_max = 100;
		
		while(!capture.isOpened())
		{
			capture = VideoCapture(0);
			count++;
			if(count>=count_max) break;
		}

		EnterCriticalSection(&cr_sec);

		capture >> image_buf;
		
		//Mat draw = Mat::zeros(334,512,CV_8UC3);
		//cv::Size s; s.height=334; s.width=512;
		//resize(image_buf,draw,s,0,0,1);
		//imshow("camera_image",draw);

		LeaveCriticalSection(&cr_sec);

		waitKey(30);

		

		//draw.release();
	}
	return 0;
}

DWORD WINAPI Camera640(PVOID param)
{

	InitializeCriticalSection(&cr_sec3);
	Mat cam_img;
	while(true)
	{
		int count = 0;
		int count_max = 100;
		
		while(!capture2.isOpened())
		{
			capture2 = VideoCapture(0);
			count++;
			if(count>=count_max) break;
		}

		EnterCriticalSection(&cr_sec3);

		capture2 >> cam_img;
		
		Mat draw = Mat::zeros(cam_img.rows/2,cam_img.cols/2,CV_8UC3);
		cv::Size s; s.height=cam_img.rows/2; s.width=cam_img.cols/2;
		resize(cam_img,draw,s,0,0,1);

		imshow("CameraImage",draw);

		LeaveCriticalSection(&cr_sec3);

		waitKey(10);
		//draw.release();
	}
	return 0;
}


void Milliteslametr::CameraConnect()
{
	InitializeCriticalSection(&cr_sec2);
	InitializeCriticalSection(&cr_sec3);
		hThread=::CreateThread(NULL,0,Camera1024,NULL,0,&dwThreadID);
		hThread2=::CreateThread(NULL,0,Camera640,NULL,0,&dwThreadID2);
}

double Milliteslametr::GetMagnetFild()
{

	int count = 0;
	int count_max = 1000;

	count = 0;

	data[0]=-1;
	data[1]=-1;
	data[2]=-1;
	data[3]=-1;

	image=imread("basic.JPG");
	
	find_of_contours(image,false);

	WINDOW=-1;
	CIRCLE=-1;
	BIG=-1;
	METKA=-1;
	SMALL=-1;

	tesla=-1;
	
	image2=image_buf.clone();
	image_buf.release();
	

	EnterCriticalSection(&cr_sec2);
	

	if(image2.rows!=0)
	{
		find_of_contours(image2,true);
		if(CIRCLE==-1||BIG==-1)
		{
			WINDOW=-1;
			CIRCLE=-1;
			BIG=-1;
			METKA=-1;
			SMALL=-1;

			keypoints_scene.clear();
			mega_new_contours.clear();
			window_hierarchy.clear();
			find_of_contours2(image2,true);
		}
	}
	cout<<BIG<<";  "<<CIRCLE<<";  "<<WINDOW<<";  "<<SMALL<<";"<<endl;
	if(keypoints_base.size()==keypoints_scene.size()&&keypoints_scene.size()!=0&&BIG!=-1&&CIRCLE!=-1&&WINDOW!=-1&&SMALL!=-1) 
		homography(image2,image);
	else
		imwrite("save_bad.jpg",image2);

	LeaveCriticalSection(&cr_sec2);
	

	new_contours.clear();
	keypoints_base.clear();
	keypoints_scene.clear();
	new_contours.clear();
	mega_new_contours.clear();
	new_contours_base.clear();
	hierar.clear();
	hierarchy.clear();
	window_hierarchy.clear();

	return tesla;

}


void Milliteslametr::find_of_contours(Mat &img,bool scene)
{
		vector<vector<Point> >contours; // vector of contours
		
		vector<vector<Point> >after_small_contours_filter; // vector of contours
		vector<Point> cnt;
		vector<vector<Point> >parent_contours; // vector of parent contours
		

		if(scene)
		{
			Mat f_img=f->homomorphic_filter(img);
			imwrite("save_fil.jpg",f_img);
			f_img.release();
			Mat f2_image=imread("save_fil.jpg");
			cvtColor(img, cvt_image, CV_RGB2GRAY);
			//f2_image.release();
			GaussianBlur(cvt_image, blu_image,Size(37,37), 0, 0);
			subtract(cvt_image, blu_image,nor_image);
			//imwrite( "sub.jpg",nor_image);
			threshold(nor_image, nor_image, 0, 255, THRESH_BINARY_INV);
			//imshow( "im2",nor_image);
		}
		else
		{
			//imshow( "im_fff",img);
			cvtColor(img, cvt_image, CV_RGB2GRAY);
			GaussianBlur(cvt_image, blu_image,Size(27,27), sigma, sigma );
			subtract(cvt_image, blu_image,nor_image);
		
		//	imwrite( "sub.jpg",nor_image);
			threshold(nor_image, nor_image, 0, 255, THRESH_BINARY_INV);
		//	imshow( "im2",nor_image);
		}
		//if(scene==true) Dilation();

		findContours(nor_image, contours, hierar, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE , Point(0, 0) );

		vector<Point2f> ContArea(contours.size());

		for( int i = 0; i < contours.size(); i++ )
		{  
			double area = contourArea(contours[i]);
			double perim = arcLength(contours[i], true );
			if(area > 70&&perim<4500)
			{
				   after_small_contours_filter.push_back(contours[i]);
			}
		}

		Mat filter_con_image = Mat::zeros(nor_image.size(), CV_8UC3 );
		for(unsigned int i = 0; i<after_small_contours_filter.size(); i++ )
		{
			   Scalar color( 255, 255, 255 );
			   drawContours( filter_con_image,  after_small_contours_filter, i, color, 1,8, hierarchy, 0, Point() );
		}

		//imshow( "Contours2", filter_con_image);
		
		cvtColor(filter_con_image, filter_con_image, CV_BGR2GRAY);
		findContours(filter_con_image, new_contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE , Point(0, 0) );
		if(scene==false) findContours(filter_con_image, new_contours_base, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE , Point(0, 0) );

		vector<vector<Point> >opor_contours;
	
		int count;
		
		Mat find_con_image = Mat::zeros(filter_con_image.size(), CV_8UC3 );
		for( int i = 0; i < new_contours.size(); i++ )
		{  
			double area =fabs( contourArea(new_contours[i]));
			double perim = arcLength( new_contours[i], true );

			if ( area / (perim * perim) > 0.05 && area / (perim * perim)< 0.87 &&
				 hierarchy[i][PARENT_CONTOUR]!=-1&&
				 perim>150&&
				 perim<3700
				 )
			{
				double parent_area=fabs( contourArea(new_contours[hierarchy[i][PARENT_CONTOUR]]));
			    if(parent_area>=3.0*area&&parent_area<=8*area)
				{
					CIRCLE=i;
					if(scene==false) CIRCLE_BASE=i;
					BIG=hierarchy[i][PARENT_CONTOUR];
					Scalar color(0, 0,255);
					Scalar color2(0, 255,0 );
					drawContours(find_con_image,new_contours, BIG, color, 1,8, hierarchy, 0, Point() );
					drawContours(find_con_image,new_contours, CIRCLE, color2, 1,8, hierarchy, 0, Point() );
				//	cout<<"perim="<<perim<<"; b="<<BIG;
					break;
				}
			}
	    
		}	

		
if((CIRCLE!=-1&&BIG!=-1)||scene==false)
{
		circle_area =fabs( contourArea(new_contours[CIRCLE]));
		circle_perim = arcLength( new_contours[CIRCLE], true );
		Moments mu_circle = moments(new_contours[CIRCLE], false );
	    Point2f mc_circle = Point2f( mu_circle.m10/ mu_circle.m00 ,  mu_circle.m01/ mu_circle.m00 );

		
		
		


		if(scene==false)
		{
			
			for( int i = 0; i < new_contours.size(); i++ )
			{  
				double area =fabs( contourArea(new_contours[i]));
				double perim = arcLength( new_contours[i], true );
				if(area>=0.5*circle_area&&area<=3*circle_area)
				{
				 
					Moments mu= moments(new_contours[i], false );
					Point2f mc=Point2f( mu.m10/ mu.m00 ,  mu.m01/ mu.m00 );
					if(scene==false) center=mc;
					float length=leng(mc_circle,mc);
               
					Scalar color( rand()&255, rand()&255, rand()&255 );
					circle( find_con_image, mc, 2, color, -1, 8, 0 );
				 
					//if(leng>circle_perim/3.14159&&leng<2.5*circle_perim/3.14159)
					if(length>0.7*circle_perim/3.14159&&length<3*circle_perim/3.14159&&
						 hierarchy[i][PARENT_CONTOUR]!=-1)
					{
						Scalar color( 255, 0, 255 );
						WINDOW_BASE=i;
						//cout<<"l="<<length<<"; ";
						drawContours(find_con_image,new_contours_base, WINDOW_BASE, color, 1,8, hierarchy, 0, Point() );
						//drawContours(find_con_image,new_contours, hierarchy[i][PARENT_CONTOUR], color, 1,8, hierarchy, 0, Point() );
					}
					
				}
				if(perim>590&&perim<600)
				{
					drawContours(find_con_image,new_contours_base, i, Scalar(35, 120,155), 1,8, hierarchy, 0, Point() );
					//cout<<"perim_con="<<perim<<"\n\n";
					SMALL_BASE=i;
				}
			}

			//imshow( "Contours55_base", find_con_image);
		}
		//imshow( "Contours55", find_con_image);
		Point top_left(0,0);
		Point top_right(0,0);
			
		Mat approx_con_image = Mat::zeros(find_con_image.size(), CV_8UC3 );
		vector <Point> vertex_big, vertex_window,vertex_big_scene,vertex_window_scene,vertex_window_scene2, vertex_small,vertex_small_scene;
		vector <vector <Point>> squares_big, squares_window, squares_small;
		double big_angle=0;
		
		if(scene==false)
		{
			keypoints_base.push_back(mc_circle);
		 
			for(int eps=0; eps<300; eps++)
			{
				approxPolyDP(new_contours[BIG], vertex_big, eps, true);
				if(vertex_big.size()==4) { break;}
			}

			for(int i=0; i<vertex_big.size(); i++)
			{
				circle( approx_con_image, vertex_big[i], 2, Scalar(255, 0, 0), 2, 8, 0 );
				
				Point p; p.x=vertex_big[i].x+5; p.y=vertex_big[i].y+5;
				char buffer[20];
                _itoa(i, buffer, 10);         // 10 - ��� ������� ���������
                std::string s = buffer; 
				putText( approx_con_image, s, p, 1,
                         1, Scalar(255,255,255), 1, 1);

				keypoints_base.push_back(vertex_big[i]);
			}

			/*for(int eps=5; eps<200; eps++)
			{
				approxPolyDP(new_contours[SMALL_BASE], vertex_small, eps, true);
				if(vertex_small.size()==4) { break;}
			}*/

			RotatedRect box = minAreaRect(new_contours[SMALL_BASE]);
			double angle = box.angle;
			big_angle=angle;
			if (angle < -45.)
				 angle += 90.;
			//cout<<"   BIG_ANGLE = "<<angle<<endl;
			Point2f vertices[4];
			box.points(vertices);
			for(int i = 0; i < 4; ++i)
				vertex_small.push_back(vertices[i]);

		    squares_small.push_back(vertex_small);

			for(int i=0; i<vertex_small.size(); i++)
			{
				circle( approx_con_image, vertex_small[i], 2, Scalar(255, 0, 0), 2, 8, 0 );
				
				Point p; p.x=vertex_small[i].x+5; p.y=vertex_small[i].y+5;
				char buffer[20];
                _itoa(i, buffer, 10);         // 10 - ��� ������� ���������
                std::string s = buffer; 
				putText( approx_con_image, s, p, 1,
                         1, Scalar(255,255,255), 1, 1);

				keypoints_base.push_back(vertex_small[i]);
			}
			/*
			top_left.x=vertex_big[2].x;
			top_left.y=vertex_big[2].y;
			top_right.x=vertex_big[3].x;
			top_right.y=vertex_big[3].y;*/
	
            drawContours(approx_con_image, squares_big, 0, Scalar(0, 255, 255), 1,8, hierarchy, 0, Point()); // fill GREEN
		
		//imshow( "base_rects",approx_con_image);
		}
		else
		{
			

			keypoints_scene.push_back(mc_circle);
		 
			for(int eps=0; eps<300; eps++)
			{
				approxPolyDP(new_contours[BIG], vertex_big_scene, eps, true);
				if(vertex_big_scene.size()==4) break;
			}
			
			float lengths[4];
			

			for(int j=0; j<vertex_big_scene.size(); j++)
			{
				lengths[0]=leng(vertex_big_scene[0],mc_circle);
				lengths[1]=leng(vertex_big_scene[1],mc_circle);
				lengths[2]=leng(vertex_big_scene[2],mc_circle);
				lengths[3]=leng(vertex_big_scene[3],mc_circle);

				if(lengths[0]>lengths[2]&&lengths[0]>lengths[3]&&lengths[1]>lengths[2]&&lengths[1]>lengths[3])
				{
					break;
				}
				else
				{
					rotate(vertex_big_scene.begin(),vertex_big_scene.begin()+1,vertex_big_scene.end());
				}
			}

			

			for(int i=0; i<vertex_big_scene.size(); i++)
			{
				circle( approx_con_image, vertex_big_scene[i], 2, Scalar(255, 0, 0), 2, 8, 0 );

				Point p; p.x=vertex_big_scene[i].x+5; p.y=vertex_big_scene[i].y+5;
				char buffer[20];
                _itoa(i, buffer, 10);         // 10 - ��� ������� ���������
                std::string s = buffer; 
				putText( approx_con_image, s, p, 1,
                         1, Scalar(255,255,255), 1, 1);

				keypoints_scene.push_back(vertex_big_scene[i]);
			}

			double max=0;
			double temp_max=0;
			for( int i = 0; i < new_contours.size(); i++ )
			{
				double perim = arcLength( new_contours[i], true );
				if(perim>circle_perim*0.95&&perim<circle_perim*1.2&&i!=CIRCLE)
				{
						Moments mu_small = moments(new_contours[i], false );
						Point2f mc_small = Point2f( mu_small.m10/ mu_small.m00 ,  mu_small.m01/ mu_small.m00 );

						if(leng(mc_circle,mc_small)>=circle_perim/2/3.14159*5&&
							leng(mc_circle,mc_small)<circle_perim/2/3.14159*8)
						{
							if(leng(vertex_big_scene[2],mc_small)<leng(vertex_big_scene[0],mc_small)&&
								leng(vertex_big_scene[2],mc_small)<leng(vertex_big_scene[1],mc_small)&&
								leng(vertex_big_scene[3],mc_small)<leng(vertex_big_scene[0],mc_small)&&
								leng(vertex_big_scene[3],mc_small)<leng(vertex_big_scene[1],mc_small)&&
								leng(vertex_big_scene[3],mc_small)>circle_perim/2/3.14159*3&&
								leng(vertex_big_scene[2],mc_small)>circle_perim/2/3.14159*3&&
								leng(vertex_big_scene[0],mc_small)>circle_perim/2/3.14159*5&&
								leng(vertex_big_scene[1],mc_small)>circle_perim/2/3.14159*5
							  )
							  {
										vertex_small.clear();
										RotatedRect box2 = minAreaRect(new_contours[i]);

										double angle = box2.angle;
										if (angle < -45.)
											 angle += 90.;

										Point2f vertices_p[4];
										box2.points(vertices_p);
										for(int i = 0; i < 4; ++i)
											vertex_small.push_back(vertices_p[i]);

										float dist1=100000;
										float dist2=100000;
										Point point_left, point_right;

										for(int i=0; i<vertex_small.size(); i++)
										{
											float d1=leng(vertex_small[i],vertex_big_scene[2]);
											float d2=leng(vertex_small[i],vertex_big_scene[3]);
											if(d1<dist1)
											{
												dist1=d1;
												point_left=vertex_small[i];
											}
											if(d2<dist2)
											{
												dist2=d2;
												point_right=vertex_small[i];
											}
										}
										vector <Point> vertex_proverka;

										vertex_proverka.push_back(vertex_big_scene[2]);
									    vertex_proverka.push_back(vertex_big_scene[3]);
										vertex_proverka.push_back(point_right);
									    vertex_proverka.push_back(point_left);

										vector <vector <Point>> squares_s;
										squares_s.push_back(vertex_proverka);

										drawContours( approx_con_image,squares_s,0, Scalar(0, 200,155), 1,8, hierarchy, 0, Point() );

										float perim_p = arcLength(vertex_proverka, true );
										cout<<"per="<<perim_p<<"; "<<"ind="<<i<<endl;

										RotatedRect box_p = minAreaRect(vertex_proverka);
										double angle_p = box_p.angle;

										if (angle_p < -45.)
											 angle_p += 90.;

										Point2f vertices_p2[4];
										box_p.points(vertices_p2);

										vector <Point> vertex_proverka_rect;

										for(int i = 0; i < 4; ++i)
											vertex_proverka_rect.push_back(vertices_p2[i]);
										
										float perim_p2 = arcLength(vertex_proverka_rect, true );
										cout<<"per_rect="<<perim_p2<<"; "<<"ind="<<i<<endl;

										squares_s.push_back(vertex_proverka_rect);

										//drawContours( approx_con_image,squares_s,1, Scalar(155, 200,0), 1,8, hierarchy, 0, Point() );


										if(perim_p<perim_p2*1.1&&perim_p>0.92*perim_p2)
										{
											max=temp_max;
											//drawContours( approx_con_image,new_contours,i, Scalar(4, 200,1), 1,8, hierarchy, 0, Point() );
											//cout<<"per="<<perim<<"; "<<"ind="<<i<<endl;
											SMALL=i;
											break;
										}
									
							}
						}
				}
			}
			if(SMALL!=-1)
			{
			vertex_small.clear();
			squares_small.clear();
			squares_small.clear();
			

			RotatedRect box = minAreaRect(new_contours[SMALL]);
			double angle = box.angle;
			big_angle=angle;
			if (angle < -45.)
				 angle += 90.;

			
	
			Point2f vertices[4];
			box.points(vertices);
			for(int i = 0; i < 4; ++i)
				vertex_small.push_back(vertices[i]);

		   

			for(int j=0; j<vertex_small.size(); j++)
			{
				lengths[0]=leng(vertex_small[0],mc_circle);
				lengths[1]=leng(vertex_small[1],mc_circle);
				lengths[2]=leng(vertex_small[2],mc_circle);
				lengths[3]=leng(vertex_small[3],mc_circle);

				if(lengths[0]>lengths[2]&&lengths[0]>lengths[3]&&lengths[1]>lengths[2]&&lengths[1]>lengths[3])
				{
					break;
				}
				else
				{
					rotate(vertex_small.begin(),vertex_small.begin()+1,vertex_small.end());
				}
			}

			 //squares_small.push_back(vertex_small);

			for(int i=0; i<vertex_small.size(); i++)
			{
				circle( approx_con_image, vertex_small[i], 2, Scalar(255, 0, 0), 2, 8, 0 );
				
				Point p; p.x=vertex_small[i].x+5; p.y=vertex_small[i].y+5;
				char buffer[20];
                _itoa(i, buffer, 10);         // 10 - ��� ������� ���������
                std::string s = buffer; 
				putText( approx_con_image, s, p, 1,
                         1, Scalar(255,255,255), 1, 1);

				keypoints_scene.push_back(vertex_small[i]);
			}

			
			drawContours( approx_con_image,new_contours,SMALL, Scalar(255, 200,155), 1,8, hierarchy, 0, Point() );
			
				 
				 drawContours(img, squares_big, 0, Scalar(0, 255, 255), 1,8, hierarchy, 0, Point()); // fill GREEN
			
			     circle( approx_con_image, mc_circle, 2, Scalar(0, 255, 0), 2, 8, 0 );
			}
		
		//imshow( "scene_rects",approx_con_image);
		}

		
		if(scene&&keypoints_base.size()==keypoints_scene.size()) 
		{
			Mat for_window_image = Mat::zeros(filter_con_image.size(), CV_8UC3 );
			
			//homography_for_window(for_window_image,image);

			Mat H2 = findHomography(keypoints_base, keypoints_scene, CV_RANSAC );

			vector <Point> vertex_window; 
			/*//coordinates of window in basic image
			vertex_window.push_back(Point(176,338));
			vertex_window.push_back(Point(177,167));
			vertex_window.push_back(Point(268,165));
			vertex_window.push_back(Point(272,333));
			*/

			vertex_window.push_back(Point(115,112));
			vertex_window.push_back(Point(233,118));
			vertex_window.push_back(Point(233,333));
			vertex_window.push_back(Point(115,333));

		    squares_window.push_back(vertex_window);

			vector <vector<Point2f>> all_contours2;
			vector <vector<Point2f>> all_contours_transform2;
			vector <vector<Point>> all_contours_transform_for_draw2;

			all_contours2.push_back(convert2Point2f_vector(squares_window[0]));

			 vector<Point2f> scene2(all_contours2[0].size());
			 perspectiveTransform(all_contours2[0],scene2, H2);

			 all_contours_transform2.push_back(scene2);
			 all_contours_transform_for_draw2.push_back(convert2Point_vector(scene2));
			 drawContours(for_window_image,all_contours_transform_for_draw2, 0, Scalar(255,255,255), 1,8, hierarchy, 0, Point() );

			 double window_area=contourArea(all_contours_transform_for_draw2[0]);
			 double window_perim = arcLength(all_contours_transform_for_draw2[0], true );
			 Moments mu_window = moments(all_contours_transform_for_draw2[0], false );
			 Point2f mc_window = Point2f( mu_window.m10/ mu_window.m00 ,  mu_window.m01/ mu_window.m00);
				
			 for(unsigned int i = 0; i<new_contours.size(); i++ )
			 {
			   double w_perim = arcLength(new_contours[i], true );
			   if(w_perim<0.8*window_perim)
				   drawContours( for_window_image,  new_contours, i, Scalar(255, 255, 255), 1,8, hierarchy, 0, Point() );
			 }
			 circle( for_window_image, mc_window, 0, Scalar(0, 255, 0), 2, 8, 0 ); 
			 
			 //imshow("UHAHA",for_window_image);

			//cout<<"ccccccccc="<<window_perim<<endl;
			Mat for_window_image3 = Mat::zeros(for_window_image.size(), CV_8UC3 );
			cvtColor(for_window_image, for_window_image, CV_BGR2GRAY); 	
			findContours(for_window_image, mega_new_contours,window_hierarchy,CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE , Point(0, 0) );
			
			for(unsigned int i = 0; i<mega_new_contours.size(); i++ )
			{		
				double s=contourArea(mega_new_contours[i]);
				double w_perim = arcLength(mega_new_contours[i], true );
				Moments mu_window2 = moments(mega_new_contours[i], false );
			    Point2f mc_window2 = Point2f( mu_window2.m10/ mu_window2.m00 ,  mu_window2.m01/ mu_window2.m00);
				
				if(s>=0.8*window_area&&window_hierarchy[i][PARENT_CONTOUR]!=-1)
				{
					circle(for_window_image3,mc_window2, 2, Scalar(0, 0, 255), 3, 8, 0 );
					circle(for_window_image3,mc_window, 2, Scalar(0, 234, 255), 3, 8, 0 );
					drawContours(for_window_image3,  mega_new_contours, i, Scalar(255, 255, 255), 1,8,window_hierarchy, 0, Point() );
					WINDOW=i;
					break;
				}
			}
			//imshow( "ssssss222",for_window_image3);
		}
		 
}
		 

		
}

void Milliteslametr::find_of_contours2(Mat &img,bool scene)
{
	vector<vector<Point> >contours; // vector of contours
		
		vector<vector<Point> >after_small_contours_filter; // vector of contours
		vector<Point> cnt;
		vector<vector<Point> >parent_contours; // vector of parent contours

		if(scene)
		{
			Mat f_img=f->homomorphic_filter(img);
			imwrite("save_fil.jpg",f_img);
			Mat f2_image=imread("save_fil.jpg");
			cvtColor(img, cvt_image, CV_RGB2GRAY);
			GaussianBlur(cvt_image, blu_image,Size(37,37), 0, 0);
			subtract(cvt_image, blu_image,nor_image);
		
			//imwrite( "sub.jpg",nor_image);
			threshold(nor_image, nor_image, 0, 255, THRESH_BINARY_INV);
			//imshow( "im2",nor_image);
		}
		else
		{
			//imshow( "im_fff",img);
			cvtColor(img, cvt_image, CV_RGB2GRAY);
			GaussianBlur(cvt_image, blu_image,Size(27,27), sigma, sigma );
			subtract(cvt_image, blu_image,nor_image);
		
		//	imwrite( "sub.jpg",nor_image);
			threshold(nor_image, nor_image, 0, 255, THRESH_BINARY_INV);
		//	imshow( "im2",nor_image);
		}
		if(scene==true) Dilation(nor_image,nor_image);

		findContours(nor_image, contours, hierar, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE , Point(0, 0) );

		vector<Point2f> ContArea(contours.size());

		for( int i = 0; i < contours.size(); i++ )
		{  
			double area = contourArea(contours[i]);
			double perim = arcLength(contours[i], true );
			if(area > 70&&perim<4500)
			{
				   after_small_contours_filter.push_back(contours[i]);
			}
		}

		Mat filter_con_image = Mat::zeros(nor_image.size(), CV_8UC3 );
		for(unsigned int i = 0; i<after_small_contours_filter.size(); i++ )
		{
			   Scalar color( 255, 255, 255 );
			   drawContours( filter_con_image,  after_small_contours_filter, i, color, 1,8, hierarchy, 0, Point() );
		}

		//imshow( "Contours2", filter_con_image);
		
		cvtColor(filter_con_image, filter_con_image, CV_BGR2GRAY);
		findContours(filter_con_image, new_contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE , Point(0, 0) );
		if(scene==false) findContours(filter_con_image, new_contours_base, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE , Point(0, 0) );

		vector<vector<Point> >opor_contours;
	
		int count;
		
		Mat find_con_image = Mat::zeros(filter_con_image.size(), CV_8UC3 );
		for( int i = 0; i < new_contours.size(); i++ )
		{  
			double area =fabs( contourArea(new_contours[i]));
			double perim = arcLength( new_contours[i], true );

			if ( area / (perim * perim) > 0.05 && area / (perim * perim)< 0.87 &&
				 hierarchy[i][PARENT_CONTOUR]!=-1&&
				 perim>150&&
				 perim<3700
				 )
			{
				double parent_area=fabs( contourArea(new_contours[hierarchy[i][PARENT_CONTOUR]]));
			    if(parent_area>=3.0*area&&parent_area<=8*area)
				{
					CIRCLE=i;
					if(scene==false) CIRCLE_BASE=i;
					BIG=hierarchy[i][PARENT_CONTOUR];
					Scalar color(0, 0,255);
					Scalar color2(0, 255,0 );
					drawContours(find_con_image,new_contours, BIG, color, 1,8, hierarchy, 0, Point() );
					drawContours(find_con_image,new_contours, CIRCLE, color2, 1,8, hierarchy, 0, Point() );
					break;
				//	cout<<"perim="<<perim<<"; b="<<BIG;
				}
			}
	    
		}	

		
if((CIRCLE!=-1&&BIG!=-1)||scene==false)
{
		circle_area =fabs( contourArea(new_contours[CIRCLE]));
		circle_perim = arcLength( new_contours[CIRCLE], true );
		Moments mu_circle = moments(new_contours[CIRCLE], false );
	    Point2f mc_circle = Point2f( mu_circle.m10/ mu_circle.m00 ,  mu_circle.m01/ mu_circle.m00 );

		
		
		


		if(scene==false)
		{
			
			for( int i = 0; i < new_contours.size(); i++ )
			{  
				double area =fabs( contourArea(new_contours[i]));
				double perim = arcLength( new_contours[i], true );
				if(area>=0.5*circle_area&&area<=3*circle_area)
				{
				 
					Moments mu= moments(new_contours[i], false );
					Point2f mc=Point2f( mu.m10/ mu.m00 ,  mu.m01/ mu.m00 );
					if(scene==false) center=mc;
					float length=leng(mc_circle,mc);
               
					Scalar color( rand()&255, rand()&255, rand()&255 );
					circle( find_con_image, mc, 2, color, -1, 8, 0 );
				 
					//if(leng>circle_perim/3.14159&&leng<2.5*circle_perim/3.14159)
					if(length>0.7*circle_perim/3.14159&&length<3*circle_perim/3.14159&&
						 hierarchy[i][PARENT_CONTOUR]!=-1)
					{
						Scalar color( 255, 0, 255 );
						WINDOW_BASE=i;
						//cout<<"l="<<length<<"; ";
						drawContours(find_con_image,new_contours_base, WINDOW_BASE, color, 1,8, hierarchy, 0, Point() );
						//drawContours(find_con_image,new_contours, hierarchy[i][PARENT_CONTOUR], color, 1,8, hierarchy, 0, Point() );
					}
					
				}
				if(perim>590&&perim<600)
				{
					drawContours(find_con_image,new_contours_base, i, Scalar(35, 120,155), 1,8, hierarchy, 0, Point() );
					//cout<<"perim_con="<<perim<<"\n\n";
					SMALL_BASE=i;
				}
			}

			//imshow( "Contours55_base", find_con_image);
		}
		//imshow( "Contours55", find_con_image);
		Point top_left(0,0);
		Point top_right(0,0);
			
		Mat approx_con_image = Mat::zeros(find_con_image.size(), CV_8UC3 );
		vector <Point> vertex_big, vertex_window,vertex_big_scene,vertex_window_scene,vertex_window_scene2, vertex_small,vertex_small_scene;
		vector <vector <Point>> squares_big, squares_window, squares_small;
		double big_angle=0;
		
		if(scene==false)
		{
			keypoints_base.push_back(mc_circle);
		 
			for(int eps=0; eps<300; eps++)
			{
				approxPolyDP(new_contours[BIG], vertex_big, eps, true);
				if(vertex_big.size()==4) { break;}
			}

			for(int i=0; i<vertex_big.size(); i++)
			{
				circle( approx_con_image, vertex_big[i], 2, Scalar(255, 0, 0), 2, 8, 0 );
				
				Point p; p.x=vertex_big[i].x+5; p.y=vertex_big[i].y+5;
				char buffer[20];
                _itoa(i, buffer, 10);         // 10 - ��� ������� ���������
                std::string s = buffer; 
				putText( approx_con_image, s, p, 1,
                         1, Scalar(255,255,255), 1, 1);

				keypoints_base.push_back(vertex_big[i]);
			}

			/*for(int eps=5; eps<200; eps++)
			{
				approxPolyDP(new_contours[SMALL_BASE], vertex_small, eps, true);
				if(vertex_small.size()==4) { break;}
			}*/

			RotatedRect box = minAreaRect(new_contours[SMALL_BASE]);
			double angle = box.angle;
			big_angle=angle;
			if (angle < -45.)
				 angle += 90.;
			//cout<<"   BIG_ANGLE = "<<angle<<endl;
			Point2f vertices[4];
			box.points(vertices);
			for(int i = 0; i < 4; ++i)
				vertex_small.push_back(vertices[i]);

		    squares_small.push_back(vertex_small);

			for(int i=0; i<vertex_small.size(); i++)
			{
				circle( approx_con_image, vertex_small[i], 2, Scalar(255, 0, 0), 2, 8, 0 );
				
				Point p; p.x=vertex_small[i].x+5; p.y=vertex_small[i].y+5;
				char buffer[20];
                _itoa(i, buffer, 10);         // 10 - ��� ������� ���������
                std::string s = buffer; 
				putText( approx_con_image, s, p, 1,
                         1, Scalar(255,255,255), 1, 1);

				keypoints_base.push_back(vertex_small[i]);
			}
			/*
			top_left.x=vertex_big[2].x;
			top_left.y=vertex_big[2].y;
			top_right.x=vertex_big[3].x;
			top_right.y=vertex_big[3].y;*/
	
            drawContours(approx_con_image, squares_big, 0, Scalar(0, 255, 255), 1,8, hierarchy, 0, Point()); // fill GREEN
		
		//imshow( "base_rects",approx_con_image);
		}
		else
		{
			

			keypoints_scene.push_back(mc_circle);
		 
			for(int eps=0; eps<300; eps++)
			{
				approxPolyDP(new_contours[BIG], vertex_big_scene, eps, true);
				if(vertex_big_scene.size()==4) break;
			}
			
			float lengths[4];
			

			for(int j=0; j<vertex_big_scene.size(); j++)
			{
				lengths[0]=leng(vertex_big_scene[0],mc_circle);
				lengths[1]=leng(vertex_big_scene[1],mc_circle);
				lengths[2]=leng(vertex_big_scene[2],mc_circle);
				lengths[3]=leng(vertex_big_scene[3],mc_circle);

				if(lengths[0]>lengths[2]&&lengths[0]>lengths[3]&&lengths[1]>lengths[2]&&lengths[1]>lengths[3])
				{
					break;
				}
				else
				{
					rotate(vertex_big_scene.begin(),vertex_big_scene.begin()+1,vertex_big_scene.end());
				}
			}

			

			for(int i=0; i<vertex_big_scene.size(); i++)
			{
				circle( approx_con_image, vertex_big_scene[i], 2, Scalar(255, 0, 0), 2, 8, 0 );

				Point p; p.x=vertex_big_scene[i].x+5; p.y=vertex_big_scene[i].y+5;
				char buffer[20];
                _itoa(i, buffer, 10);         // 10 - ��� ������� ���������
                std::string s = buffer; 
				putText( approx_con_image, s, p, 1,
                         1, Scalar(255,255,255), 1, 1);

				keypoints_scene.push_back(vertex_big_scene[i]);
			}

			double max=0;
			double temp_max=0;
			for( int i = 0; i < new_contours.size(); i++ )
			{
				double perim = arcLength( new_contours[i], true );
				if(perim>circle_perim*0.95&&perim<circle_perim*1.2&&i!=CIRCLE)
				{
						Moments mu_small = moments(new_contours[i], false );
						Point2f mc_small = Point2f( mu_small.m10/ mu_small.m00 ,  mu_small.m01/ mu_small.m00 );

						if(leng(mc_circle,mc_small)>=circle_perim/2/3.14159*5&&
							leng(mc_circle,mc_small)<circle_perim/2/3.14159*8)
						{
							if(leng(vertex_big_scene[2],mc_small)<leng(vertex_big_scene[0],mc_small)&&
								leng(vertex_big_scene[2],mc_small)<leng(vertex_big_scene[1],mc_small)&&
								leng(vertex_big_scene[3],mc_small)<leng(vertex_big_scene[0],mc_small)&&
								leng(vertex_big_scene[3],mc_small)<leng(vertex_big_scene[1],mc_small)&&
								leng(vertex_big_scene[3],mc_small)>circle_perim/2/3.14159*3&&
								leng(vertex_big_scene[2],mc_small)>circle_perim/2/3.14159*3&&
								leng(vertex_big_scene[0],mc_small)>circle_perim/2/3.14159*5&&
								leng(vertex_big_scene[1],mc_small)>circle_perim/2/3.14159*5
							  )
							  {
										vertex_small.clear();
										RotatedRect box2 = minAreaRect(new_contours[i]);

										double angle = box2.angle;
										if (angle < -45.)
											 angle += 90.;

										Point2f vertices_p[4];
										box2.points(vertices_p);
										for(int i = 0; i < 4; ++i)
											vertex_small.push_back(vertices_p[i]);

										float dist1=100000;
										float dist2=100000;
										Point point_left, point_right;

										for(int i=0; i<vertex_small.size(); i++)
										{
											float d1=leng(vertex_small[i],vertex_big_scene[2]);
											float d2=leng(vertex_small[i],vertex_big_scene[3]);
											if(d1<dist1)
											{
												dist1=d1;
												point_left=vertex_small[i];
											}
											if(d2<dist2)
											{
												dist2=d2;
												point_right=vertex_small[i];
											}
										}
										vector <Point> vertex_proverka;

										vertex_proverka.push_back(vertex_big_scene[2]);
									    vertex_proverka.push_back(vertex_big_scene[3]);
										vertex_proverka.push_back(point_right);
									    vertex_proverka.push_back(point_left);

										vector <vector <Point>> squares_s;
										squares_s.push_back(vertex_proverka);

										drawContours( approx_con_image,squares_s,0, Scalar(0, 200,155), 1,8, hierarchy, 0, Point() );

										float perim_p = arcLength(vertex_proverka, true );
										cout<<"per="<<perim_p<<"; "<<"ind="<<i<<endl;

										RotatedRect box_p = minAreaRect(vertex_proverka);
										double angle_p = box_p.angle;

										if (angle_p < -45.)
											 angle_p += 90.;

										Point2f vertices_p2[4];
										box_p.points(vertices_p2);

										vector <Point> vertex_proverka_rect;

										for(int i = 0; i < 4; ++i)
											vertex_proverka_rect.push_back(vertices_p2[i]);
										
										float perim_p2 = arcLength(vertex_proverka_rect, true );
										cout<<"per_rect="<<perim_p2<<"; "<<"ind="<<i<<endl;

										//squares_s.push_back(vertex_proverka_rect);

										//drawContours( approx_con_image,squares_s,1, Scalar(155, 200,0), 1,8, hierarchy, 0, Point() );


										if(perim_p<perim_p2*1.1&&perim_p>0.92*perim_p2)
										{
											max=temp_max;
											//drawContours( approx_con_image,new_contours,i, Scalar(4, 200,1), 1,8, hierarchy, 0, Point() );
											//cout<<"per="<<perim<<"; "<<"ind="<<i<<endl;
											SMALL=i;
											break;
										}
									
							}
						}
				}
			}
			if(SMALL!=-1)
			{
			vertex_small.clear();
			squares_small.clear();
			squares_small.clear();
			

			RotatedRect box = minAreaRect(new_contours[SMALL]);
			double angle = box.angle;
			big_angle=angle;
			if (angle < -45.)
				 angle += 90.;

			
	
			Point2f vertices[4];
			box.points(vertices);
			for(int i = 0; i < 4; ++i)
				vertex_small.push_back(vertices[i]);

		   

			for(int j=0; j<vertex_small.size(); j++)
			{
				lengths[0]=leng(vertex_small[0],mc_circle);
				lengths[1]=leng(vertex_small[1],mc_circle);
				lengths[2]=leng(vertex_small[2],mc_circle);
				lengths[3]=leng(vertex_small[3],mc_circle);

				if(lengths[0]>lengths[2]&&lengths[0]>lengths[3]&&lengths[1]>lengths[2]&&lengths[1]>lengths[3])
				{
					break;
				}
				else
				{
					rotate(vertex_small.begin(),vertex_small.begin()+1,vertex_small.end());
				}
			}

			 //squares_small.push_back(vertex_small);

			for(int i=0; i<vertex_small.size(); i++)
			{
				circle( approx_con_image, vertex_small[i], 2, Scalar(255, 0, 0), 2, 8, 0 );
				
				Point p; p.x=vertex_small[i].x+5; p.y=vertex_small[i].y+5;
				char buffer[20];
                _itoa(i, buffer, 10);         // 10 - ��� ������� ���������
                std::string s = buffer; 
				putText( approx_con_image, s, p, 1,
                         1, Scalar(255,255,255), 1, 1);

				keypoints_scene.push_back(vertex_small[i]);
			}

			
			drawContours( approx_con_image,new_contours,SMALL, Scalar(255, 200,155), 1,8, hierarchy, 0, Point() );
			
				 
				 drawContours(img, squares_big, 0, Scalar(0, 255, 255), 1,8, hierarchy, 0, Point()); // fill GREEN
			
			     circle( approx_con_image, mc_circle, 2, Scalar(0, 255, 0), 2, 8, 0 );
			}
		
		//imshow( "scene_rects",approx_con_image);
		}

		
		if(scene&&keypoints_base.size()==keypoints_scene.size()) 
		{
			Mat for_window_image = Mat::zeros(filter_con_image.size(), CV_8UC3 );
			
			//homography_for_window(for_window_image,image);

			Mat H2 = findHomography(keypoints_base, keypoints_scene, CV_RANSAC );

			vector <Point> vertex_window; 

			vertex_window.push_back(Point(115,112));
			vertex_window.push_back(Point(233,118));
			vertex_window.push_back(Point(233,333));
			vertex_window.push_back(Point(115,333));

		    squares_window.push_back(vertex_window);

			vector <vector<Point2f>> all_contours2;
			vector <vector<Point2f>> all_contours_transform2;
			vector <vector<Point>> all_contours_transform_for_draw2;

			all_contours2.push_back(convert2Point2f_vector(squares_window[0]));

			 vector<Point2f> scene2(all_contours2[0].size());
			 perspectiveTransform(all_contours2[0],scene2, H2);

			 all_contours_transform2.push_back(scene2);
			 all_contours_transform_for_draw2.push_back(convert2Point_vector(scene2));
			 drawContours(for_window_image,all_contours_transform_for_draw2, 0, Scalar(255,255,255), 1,8, hierarchy, 0, Point() );

			 double window_area=contourArea(all_contours_transform_for_draw2[0]);
			 double window_perim = arcLength(all_contours_transform_for_draw2[0], true );
			 Moments mu_window = moments(all_contours_transform_for_draw2[0], false );
			 Point2f mc_window = Point2f( mu_window.m10/ mu_window.m00 ,  mu_window.m01/ mu_window.m00);
				
			 for(unsigned int i = 0; i<new_contours.size(); i++ )
			 {
			   double w_perim = arcLength(new_contours[i], true );
			   if(w_perim<0.8*window_perim)
				   drawContours( for_window_image,  new_contours, i, Scalar(255, 255, 255), 1,8, hierarchy, 0, Point() );
			 }
			 circle( for_window_image, mc_window, 0, Scalar(0, 255, 0), 2, 8, 0 ); 
			 
			 //imshow("UHAHA",for_window_image);

			//cout<<"ccccccccc="<<window_perim<<endl;
			Mat for_window_image3 = Mat::zeros(for_window_image.size(), CV_8UC3 );
			cvtColor(for_window_image, for_window_image, CV_BGR2GRAY); 	
			findContours(for_window_image, mega_new_contours,window_hierarchy,CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE , Point(0, 0) );
			
			for(unsigned int i = 0; i<mega_new_contours.size(); i++ )
			{		
				double s=contourArea(mega_new_contours[i]);
				double w_perim = arcLength(mega_new_contours[i], true );
				Moments mu_window2 = moments(mega_new_contours[i], false );
			    Point2f mc_window2 = Point2f( mu_window2.m10/ mu_window2.m00 ,  mu_window2.m01/ mu_window2.m00);
				
				if(s>=0.8*window_area&&window_hierarchy[i][PARENT_CONTOUR]!=-1)
				{
					circle(for_window_image3,mc_window2, 2, Scalar(0, 0, 255), 3, 8, 0 );
					circle(for_window_image3,mc_window, 2, Scalar(0, 234, 255), 3, 8, 0 );
					drawContours(for_window_image3,  mega_new_contours, i, Scalar(255, 255, 255), 1,8,window_hierarchy, 0, Point() );
					WINDOW=i;
				}
			}
			//imshow( "ssssss222",for_window_image3);
		}

	
		}
		
}

void Milliteslametr::homography_for_window(Mat &img_scene,Mat &img_base)
{

	//cout<<"++++++++++++"<<endl;
	Mat H2 = findHomography(keypoints_base, keypoints_scene, CV_RANSAC );

	vector <Point> vertex_window; 	
			vector <vector <Point>> squares_window;
			vertex_window.push_back(Point(172,338));
			vertex_window.push_back(Point(173,167));
			vertex_window.push_back(Point(268,165));
			vertex_window.push_back(Point(272,333));
		    squares_window.push_back(vertex_window);

	vector <vector<Point2f>> all_contours2;
	vector <vector<Point2f>> all_contours_transform2;
	vector <vector<Point>> all_contours_transform_for_draw2;

	all_contours2.push_back(convert2Point2f_vector(squares_window[0]));

	 vector<Point2f> scene2(all_contours2[0].size());
	 perspectiveTransform(all_contours2[0],scene2, H2);

	 all_contours_transform2.push_back(scene2);
		all_contours_transform_for_draw2.push_back(convert2Point_vector(scene2));
		drawContours(img_scene,all_contours_transform_for_draw2, 0, Scalar(255,255,255), 1,8, hierarchy, 0, Point() );
	//	imshow("UHAHA",img_scene);
		
}
	
void Milliteslametr::homography(Mat &img_scene,Mat &img_base)
{

	Mat H = findHomography(keypoints_scene, keypoints_base, CV_RANSAC );

	vector <vector<Point2f>> all_contours;
	vector <vector<Point2f>> all_contours_transform;
	vector <vector<Point>> all_contours_transform_for_draw;
	vector <vector<Point2f>> eight_contours;
	vector <vector<Point2f>> eight_contours_transform;
	vector <vector<Point>> eight_contours_transform_for_draw;
	vector <vector<Point>> buttons_contours_transform_for_draw;
	vector <vector<Point>> metka_contours_transform_for_draw;

	double win_area=fabs( contourArea(mega_new_contours[WINDOW]));
	vector <int> index_of_num_contours;
	for(int i = 0; i < mega_new_contours.size(); i++ )
	{
		double area =fabs( contourArea(mega_new_contours[i]));
		
		if(window_hierarchy[i][PARENT_CONTOUR]==WINDOW&&area>0.005*win_area)
		{
			all_contours.push_back(convert2Point2f_vector(mega_new_contours[i]));
			//Scalar color(255,0,0);
			//drawContours(img_scene,mega_new_contours, i, color, 1,8,window_hierarchy, 0, Point() );
			index_of_num_contours.push_back(i);
		}
	}

	for(int i = 0; i < mega_new_contours.size(); i++ )
	{
		Moments mu= moments(mega_new_contours[i], false );
		Point2f mc=Point2f( mu.m10/mu.m00,mu.m01/mu.m00);
		
		if(pointPolygonTest(mega_new_contours[WINDOW],mc,false)>0)
		{
			if(window_hierarchy[i][FIRST_CHILD_CONTOUR]==-1&&window_hierarchy[i][PARENT_CONTOUR]!=WINDOW
				&&arcLength(mega_new_contours[window_hierarchy[i][PARENT_CONTOUR]], true )<1.05*arcLength(mega_new_contours[i],true)
				)
					{
						if(pointPolygonTest(mega_new_contours[i],mc,false)>0)	
						{
							eight_contours.push_back(convert2Point2f_vector(mega_new_contours[i]));	
						}
					}
		}
	}
	int count1=0;
	for(int i=0; i<all_contours.size(); i++)
	{
        vector<Point2f> scene(all_contours[i].size());
		perspectiveTransform(all_contours[i],scene, H);
		all_contours_transform.push_back(scene);
		
		all_contours_transform_for_draw.push_back(convert2Point_vector(all_contours_transform[count1]));
		drawContours(img_base,all_contours_transform_for_draw, count1, Scalar(255,0,0), 1,8, hierarchy, 0, Point() );
		count1++;
	}
	int count2=0;
	for(int i=0; i<eight_contours.size(); i++)
	{
        vector<Point2f> scene(eight_contours[i].size());
		perspectiveTransform(eight_contours[i],scene, H);
		eight_contours_transform.push_back(scene);
		eight_contours_transform_for_draw.push_back(convert2Point_vector(eight_contours_transform[count2]));
		drawContours(img_base,eight_contours_transform_for_draw, i, Scalar(0,255,0), 1,8, 0, 0, Point() );
		count2++;
	}

		data[0]=first_numeric_detector(all_contours_transform_for_draw,eight_contours_transform_for_draw);
		data[1]=second_numeric_detector(all_contours_transform_for_draw,eight_contours_transform_for_draw);
		data[2]=third_numeric_detector(all_contours_transform_for_draw,eight_contours_transform_for_draw);
		data[3]=fourth_numeric_detector(all_contours_transform_for_draw,eight_contours_transform_for_draw);

		if(data[1]==-1||data[2]==-1||data[3]==-1)
			imwrite("save.jpg",img_scene);
		if(data[1]!=-1&&data[2]!=-1&&data[3]!=-1)
			imwrite("save_good.jpg",img_scene);

			
		if(data[1]!=-1&&data[2]!=-1&&data[3]!=-1&&data[0]==-1)
			tesla=data[1]*100+data[2]*10+data[3];
		else 
			tesla=-1;
		if(data[0]==-1&&data[1]==-1&&data[2]!=-1&&data[3]!=-1)
			tesla=data[2]*10+data[3];  
		if(data[0]==-1&&data[1]==-1&&data[2]==-1&&data[3]!=-1)
			tesla=data[3];
		if(data[1]!=-1&&data[2]!=-1&&data[3]!=-1&&data[0]!=-1)
			tesla=data[0]*1000+data[1]*100+data[2]*10+data[3];
 
}

void Milliteslametr::Erosion(Mat &img_src,Mat &img_dst)
{
  static const int erosion_elem = 0;
  static const int erosion_size = 1;
  int erosion_type;
  if( erosion_elem == 0 ){ erosion_type = MORPH_RECT; }
  else if( erosion_elem == 1 ){ erosion_type = MORPH_CROSS; }
  else if( erosion_elem == 2) { erosion_type = MORPH_ELLIPSE; }

  Mat element = getStructuringElement( erosion_type,
                                       Size( erosion_size + 1, erosion_size+1 ),
                                       Point( erosion_size, erosion_size ) );

  /// Apply the erosion operation
  erode(img_src,img_dst, element );

}

void Milliteslametr::Dilation(Mat &img_src,Mat &img_dst)
{
	static const int dilation_elem = 0;
	static const int dilation_size = 1;
    int dilation_type;
    if( dilation_elem == 0 ){ dilation_type = MORPH_RECT; }
    else if( dilation_elem == 1 ){ dilation_type = MORPH_CROSS; }
    else if( dilation_elem == 2) { dilation_type = MORPH_ELLIPSE; }

    Mat element = getStructuringElement( dilation_type,
                                       Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                       Point( dilation_size, dilation_size ) );
  /// Apply the dilation operation
    dilate( nor_image, nor_image, element );
 // imshow( "Dilation Demo", nor_image );
}



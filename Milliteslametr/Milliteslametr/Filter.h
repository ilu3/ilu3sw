//**************************************************************
//�����������:      ������� ���� ����, �. ������ ��������
//������:	        ��� ���-3
//������ �������:   1.0
//������������:     ANSI C++ 98
//������:   		22.04.2014
//�������:   		27.09.2015
//**************************************************************/

/** \author ��������� �.�.*/
/** \file Filter.h*/
/** \brief �������� ������� ��� ���������� �����������.*/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <Windows.h>
#include <fstream>
#include <numeric>

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <opencv2\imgproc\imgproc_c.h>

#include <algorithm>
using namespace cv;

class Filter
{
public:
	//! �����������
	Filter()
	{
	};
	//! ����������
	~Filter()
	{
	};
	//! ����������� ���������� �����������
	/*! ������� ��������� �������� ���������� ��������� �����������. �������������� ���������������� �����������, ������ �������������� �����,
		���������� �������������� ��������, �������� �������������� ����� � ��������������� �����������.
		\param [in] in_img ������� �����������
		\return ��������������� �����������
	*/
	Mat homomorphic_filter(Mat &in_img);
	
private:
	//! ������������ �����-�����
	/*! ������� ������������ 1-3, 2-4 ��������� ��������� �����-������, ����� �������
		�������������� ���������� ����������� ������������ � ����� ���������� �����-������.
		\param [in] src_arr ������� ����������� �����-������
		\param [out] dst_arr �������������� �����-�����
	*/
	void cvShiftDFT(CvArr * src_arr, CvArr * dst_arr );
	//! ������� ������ �����������
	/*! ������� ���������� ��������� ������ �����������, ������������
		��� ���������� ����������� � ��������� �������.
		\param [in] D ������� �����
		\param [in] n ������� �������
		\param [in] upper ������� �����
		\param [in] lower ������ �����
		\param [out] dft_Filter ��������� ������
	*/
	void create_butterworth_homomorphic_filter(CvMat* dft_Filter, int D, int n, float upper, float lower);
};

